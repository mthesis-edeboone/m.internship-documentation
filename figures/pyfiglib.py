"""
Various functions usable by multiple scripts
"""
from matplotlib import rcParams
import matplotlib.pyplot as plt
import os.path as path
import numpy as np

rcParams["text.usetex"] = True
rcParams["font.family"] = "serif"
rcParams["font.size"] = "12"
rcParams["grid.linestyle"] = 'dotted'
