% vim: fdm=marker fmr=<<<,>>>
%%
%% Thesis Preamble
%%

%% <<< Packages
%%

% For Bibliography
\usepackage[sorting=none,natbib=true,citestyle=numeric-comp,backend=bibtex]{biblatex}

% Document
\usepackage{amsmath}
\usepackage{mathtools}
%\usepackage{cancel}
\usepackage{todo}

% Graphics
\usepackage{placeins}
\usepackage{xcolor}
\usepackage{graphicx}
\usepackage{epstopdf}

\usepackage{caption}
\usepackage{subcaption}
\captionsetup{subrefformat=parens}
\usepackage{enumitem}

\usepackage[colorlinks=true]{hyperref}
\usepackage{cleveref}
\usepackage{grffile}
\usepackage{physics}

\usepackage[english]{babel}       % for proper word breaking at line ends
\usepackage[switch]{lineno}
\usepackage[acronym]{glossaries}
\glsdisablehyper

%\usepackage{listings}
\usepackage{csquotes}
\usepackage[defaultlines=3,all]{nowidow}
\usepackage[margin=1.0in]{geometry}
\usepackage[nobottomtitles*]{titlesec}
\usepackage{subfiles}


%% >>>
\newcommand{\email}[1]{\href{mailto:#1}{#1}}
%% <<< Layout
%%

\titleformat{\chapter}[block] % shape
{\normalfont\bfseries\LARGE\filcenter} % format
{\chaptertitlename\ \thechapter.\\} % label
{0.5ex} % sep
{\huge
} % before-code
[\vspace{0.5ex}%
{\titlerule[2pt]}
] % after-code

\titlespacing{\chapter}{0pt}{*3}{*3}

\setlength{\parindent}{0cm}

%\DeclareRobustCommand{\thefootnote}{\alph{footnote}} % alphabetic footnote markers

%% Automatic float barriers for section and subsection
\let\Oldsection\section
\renewcommand{\section}{\FloatBarrier\Oldsection}

%\let\Oldsubsection\subsection
%\renewcommand{\subsection}{\FloatBarrier\Oldsubsection}

%\let\Oldsubsubsection\subsubsection
%\renewcommand{\subsubsection}{\FloatBarrier\Oldsubsubsection}

%% >>>
%% <<< Shortcuts
%%

\newcommand*{\captionsource}[2]{%
  \caption[{#1}]{%
    #1%
    \\\hspace{\linewidth}%
    \textbf{Source:} #2%
  }%
}

% <<<< Math
\newcommand{\dif}[1]{\mathop{}\!\mathrm{d} #1}
\newcommand{\pdif}[1]{\mathop{}\!\mathrm{\partial} #1}
\newcommand{\dbyd}[2]{\ensuremath{\mathrm{d}{#1}/\mathrm{d}{#2}}}

\DeclareMathOperator{\Corr}{Corr}
%\DeclareMathOperator{\erf}{erf}
\DeclareMathOperator{\argmax}{argmax}
\DeclareMathOperator{\arctantwo}{arctan2}

\DeclareRobustCommand{\eexp}[1]{\ensuremath{e^{#1}}}
\DeclareRobustCommand{\expOf}[1]{\ensuremath{\exp{\!\left(#1\right)}}}
\DeclareRobustCommand{\ofOrder}[1]{\ensuremath{\mathcal{O}\left(#1\right)}}

% >>>>
% <<<< Units
\newcommand{\eV}{\text{\,e\kern-0.15exV}}
\newcommand{\MeV}{\text{\,M\!\eV}}
\newcommand{\GeV}{\text{\,G\!\eV}}
\newcommand{\TeV}{\text{\,T\!\kern-0.1ex\eV}}

\newcommand{\metre}{\text{\,m}}

\newcommand{\mV}{\text{\,m\kern-0.1exV}}
\newcommand{\uV}{\text{\,\textmu\kern-0.1exV}}

\newcommand{\uVperm}{\text{\uV\kern-0.2ex/\kern-0.1ex\!\metre}}

\newcommand{\ns}{\text{\,ns}}
\newcommand{\us}{\text{\,\textmu s}}

\newcommand{\kHz}{\text{\,kHz}}
\newcommand{\MHz}{\text{\,MHz}}
\newcommand{\GHz}{\text{\,GHz}}
% >>>>
% <<<< Quantities
% Notes:
% \tau is a measured/apparent quantity
% t is true time
% priming is required for moving with the signal / different reference frame

\newcommand{\beaconfreq}{\ensuremath{f_\mathrm{beacon}}}
\newcommand{\fbeacon}{\ensuremath{f_\mathrm{beacon}}}

\newcommand{\Xmax}{\ensuremath{X_\mathrm{max}}}

\newcommand{\phase}{\varphi} % deprecated
\newcommand{\tTime}{t} % deprecated

%% time variables
\newcommand{\tTrue}{t}
\newcommand{\tMeas}{\tau}
\newcommand{\tRes}{\tTrue_\mathrm{res}}
\newcommand{\tResidual}{\tRes}
\newcommand{\tTrueTrue}{\tTrue_\mathrm{true}}

\newcommand{\tTrueEmit}{\tTrue_0}
\newcommand{\tTrueArriv}{\tTrueEmit'}
\newcommand{\tMeasArriv}{\tMeas_0}
\newcommand{\tProp}{\tTrue_d}
\newcommand{\tClock}{\tTrue_c}
\newcommand{\tClockPhase}{\ensuremath{\tTrue_\phase}}
\newcommand{\tClockPeriod}{\ensuremath{k T}}

%% phase variables
\newcommand{\pTrue}{\phi}
\newcommand{\PTrue}{\Phi}
\newcommand{\pMeas}{\varphi}
\newcommand{\pRes}{\pTrue_\mathrm{res}}
\newcommand{\pResidual}{\pRes}
\newcommand{\pTrueTrue}{\pTrue_\mathrm{true}}

\newcommand{\pTrueEmit}{\pTrue_0}
\newcommand{\pTrueArriv}{\pTrueArriv'}
\newcommand{\pMeasArriv}{\pMeas_0}
\newcommand{\pProp}{\pTrue_d}
\newcommand{\pClock}{\pTrue_c}

%% >>>>
%% >>>
%% <<< Acronyms
%%
\newacronym{GNSS}{GNSS}{Global Navigation Satellite System}
\newacronym{EAS}{EAS}{Extensive Air Shower}
\newacronym{UHE}{UHE}{Ultra High Energy}
\newacronym{UHECR}{UHECR}{\acrlong{UHE} Cosmic Ray}
\newacronym{DU}{DU}{Detector Unit}
\newacronym{PPS}{PPS}{Pulse Per Second}

%% <<<< Names
\newacronym{GRAND}{GRAND}{Giant Radio Array for Neutrino Detection}
\newacronym{BEACON}{BEACON}{Beamforming Elevated Array for COsmic Neutrinos}
\newacronym{LOFAR}{LOFAR}{Low-Frequency Array}
\newacronym{PA}{PA}{Pierre~Auger}
\newacronym{PAObs}{PAO}{Pierre~Auger Observatory}
\newacronym{Auger}{Auger}{\acrlong{PAObs}}
\newacronym{AugerPrime}{AugerPrime}{AugerPrime}
\newacronym{AERA}{\textsc{AERA}}{Auger Engineering Radio~Array}

\newacronym{ADC}{\textsc{ADC}}{Analog-to-Digital~Converter}
\newacronym{ZHAireS}{ZHAireS}{ZHAireS}
%% >>>>
%% <<<< Math
\newacronym{DTFT}{\textsc{DTFT}}{Discrete Time Fourier Transform}
\newacronym{DFT}{\textsc{DFT}}{Discrete Fourier Transform}
\newacronym{FFT}{\textsc{FFT}}{Fast Fourier Transform}
\newacronym{FT}{\textsc{FT}}{Fourier Transform}

\newacronym{RMS}{\textsc{RMS}}{root-mean-square}
\newacronym{SNR}{\textsc{SNR}}{signal-to-noise ratio}

%% >>>>
%% >>>
