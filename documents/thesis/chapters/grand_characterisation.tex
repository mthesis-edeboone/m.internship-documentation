% vim: fdm=marker fmr=<<<,>>>
\documentclass[../thesis.tex]{subfiles}

\graphicspath{
	{.}
	{../../figures/}
	{../../../figures/}
}

\begin{document}
\chapter{GRAND signal chain characterisation}
\label{sec:gnss_accuracy}

% systematic delays important to obtain the best synchronisation
The beacon synchronisation strategy hinges on the ability to measure the beacon signal with sufficient timing accuracy.
In the previous chapters, the overall performance of this strategy has been explored by using simulated waveforms.
\\
% ADC and filtering setup most important component.
As mentioned in Chapter~\ref{sec:waveform}, the measured waveforms of a true detector will be influenced by characteristics of the antenna, the filter and the \gls{ADC}.
Especially the filter and \gls{ADC} are important components to be characterised to compensate for possible systematic (relative) delays.
This chapter starts an investigation into these systematic delays within \gls{GRAND}'s \gls{DU} V2.0\cite{GRAND:DU2}.
\\

%\section{GRAND DU}% <<<
%\begin{figure}
%	\begin{subfigure}{0.47\textwidth}
%		\includegraphics[width=\textwidth]{grand/DU_board_encased}
%	\end{subfigure}
%	\hfill
%	\begin{subfigure}{0.47\textwidth}
%		\includegraphics[width=\textwidth]{grand/DU_board_nocase}
%	\end{subfigure}
%	\caption{
%		\gls{GRAND}'s \acrlong{DU} V2.0 inside (\textit{left}) and outside (\textit{right}) its protective encasing.
%	}
%	\label{fig:grand_du}
%\end{figure}

% ADC
At the base of every single antenna, a \gls{DU} is mounted.
Its protective encasing has three inputs to which the different polarisations of the antenna are connected.
These inputs are connected to their respective filter chains, leaving a fourth filter chain as spare.
Each filter chain band-passes the signal between $30\MHz$ and $200\MHz$.
Finally, the signals are digitised by a four channel 14-bit \gls{ADC} sampling at $500\MHz$.
%The input voltage ranges from $-900\mV$ to $+900\mV$.
In our setup, the channels are read out after one of two internal ``monitoring'' triggers fire with the ten-second trigger (TD) linked to the 1~\acrlong{PPS} of the \gls{GNSS} chip and the other (MD) a variable randomising trigger.
\\

% timestamp = GPS + local oscillator
%The \gls{DU} timestamps an event using a combination of the 1\gls{PPS} of a Trimble ICM 360 \gls{GNSS} chip and counting the local oscillator running at $500\MHz$.
%At trigger time, the counter value is stored to obtain a timing accuracy of roughly $2\ns$.
%The counter is also used to correct for fluctuating intervals of the 1\gls{PPS} by storing and resetting it at each incoming 1\gls{PPS}.


\begin{figure}% <<<<
	\centering
		\includegraphics[width=0.5\textwidth]{grand/DU/1697110935017.jpeg}
		\caption{
			\gls{GRAND}'s \acrlong{DU} V2.0 inside its protective encasing.
		}
		\label{fig:grand_du}
\end{figure}% >>>>

% >>>
%\section{Filterchain Relative Time Delays}% <<<
Both the \gls{ADC} and the filter chains introduce systematic delays.
Since each channel corresponds to a polarisation, it is important that relative systematic delays between the channels can be accounted for.
\\

\begin{figure}
	\centering
	\includegraphics[width=0.4\textwidth]{grand/setup/channel-delay-setup.pdf}
	\caption{
		Relative time delay experiment, a signal generator sends the same signal to two channels of the \gls{DU}.
		The extra time delay incurred by the loop in the upper cable can be ignored by interchanging the cabling and doing a second measurement.
	}
	\label{fig:channel-delay-setup}
\end{figure}
Figure~\ref{fig:channel-delay-setup} illustrates a setup to measure the relative time delays of the filter chain and \gls{ADC}.
Two \gls{DU}-channels receive the same signal from a signal generator where one of the channels takes an extra time delay $\Delta t_\mathrm{cable}$ due to extra cable length.
In this ``forward'' setup, both channels are read out at the same time, and a time delay is derived from the channels' traces.
Afterwards, the cables are interchanged and a second (``backward'') time delay is measured.
\\
The sum of the ``forward'' and ``backward'' time delays gives twice the relative time delay $\Delta t$ without needing to measure the time delays due to the cable lengths $t_\mathrm{cable}$ separately since
\begin{equation}\label{eq:forward_backward_cabling}
	\phantom{.}
	\Delta t
	= (t_\mathrm{forward} + t_\mathrm{backward})/2
	= ([\Delta t + t_\mathrm{cable}] + [\Delta t - t_\mathrm{cable}])/2
	.
\end{equation}\\

% setup: signal
We used a signal generator to emit a single sine wave at frequencies from $50\MHz$ to $200\MHz$ at $200\;\mathrm{mVpp}$.
Note that we measured the phases to determine the time delays for each channel.
In Figure~\ref{fig:grand:signal} the time delay between the channels is clearly visible in the measured waveforms as well as in the phase spectrum.
\\
\begin{figure}% <<< fig:grand:signal
	\begin{subfigure}{0.47\textwidth}
		\includegraphics[width=\textwidth]{grand/split-cable/waveform_eid1_ch1ch2.pdf}
		\label{fig:split-cable:waveform}
	\end{subfigure}
	\hfill
	\begin{subfigure}{0.47\textwidth}
		\includegraphics[width=\textwidth]{grand/split-cable/waveform_eid1_ch1ch2_spectrum.pdf}
		\label{fig:split-cable:waveform:spectra}
	\end{subfigure}
	\caption{
		Waveforms of the sine wave measured in the ``forward'' setup and their spectra around the testing frequency of $50\MHz$..
		The sine wave was emitted at $200\;\mathrm{mVpp}$.
	}
	\label{fig:grand:signal}
\end{figure}% >>>

% Frequencies above 50mhz not true measurement
In our setup, the cable length difference was $3.17-2.01 = 1.06\metre$, resulting in an estimated cable time delay of roughly $5\ns$.
At a frequency of $50\MHz$, the difference between the forward and backward phase differences is thus expected to be approximately half a cycle.
Figures~\ref{fig:grand:phaseshift:measurements} and~\ref{fig:grand:phaseshift} show this is in accordance with the measured delays.
\\

\begin{figure}% <<< fig:grand:phaseshift:measurements
	\centering
	\begin{subfigure}{0.47\textwidth}
		\includegraphics[width=\textwidth]{grand/split-cable/sine-sweep/ch2ch4fig9-measurements.forward.50.pdf}
		\caption{}
		\label{fig:grand:phaseshift:measurements:forward}
	\end{subfigure}
	\hfill
	\begin{subfigure}{0.47\textwidth}
		\includegraphics[width=\textwidth]{grand/split-cable/sine-sweep/ch2ch4fig9-measurements.backward.50.pdf}
		\caption{}
		\label{fig:grand:phaseshift:measurements:backward}
	\end{subfigure}
	\caption{
		The measured phase differences between channels 2 and 4 at $50\MHz$ converted to a time delay for the \subref{fig:grand:phaseshift:measurements:forward}~forward and \subref{fig:grand:phaseshift:measurements:backward}~backward setups.
		The dashed vertical lines indicate the mean time delay, the errorbar at the bottom indicates the standard deviation of the samples.
		Crosses are TD-triggered events, circles are MD-triggered.
		The measurements are time-ordered within their trigger type.
	}
	\label{fig:grand:phaseshift:measurements}
\end{figure}

\begin{figure}% <<< fig:grand:phaseshift
	\centering
	\includegraphics[width=0.47\textwidth]{grand/split-cable/sine-sweep/ch2ch4fig8-histogram.50.pdf}
	\caption{
		Histogram of the measured phase differences in Figure~\ref{fig:grand:phaseshift:measurements}.
		The relative signal chain time delay for the portrayed means is $0.2\ns$.
	}
	\label{fig:grand:phaseshift}
\end{figure}% >>>

\clearpage

% Conclusion
Figure~\ref{fig:channel-delays} shows the measured total time delays and the resulting signal chain time delays between both channels 1 and 2, and channels 2 and 4.
Apart from two exceptional time delays up to $0.2\ns$, the signal chain time delays are in general below $0.05\ns$.
\\
Note that the reported signal chain time delays must be taken to be indications due to systematic behaviours (see below).
\\
Still, even when taking $0.2\ns$ as the upper limit of any relative signal chain time delay, the electric field at the antenna are reconstructable to a sufficient accuracy to use either the pulsed or sine beacon methods (see Figures~\ref{fig:pulse:snr_time_resolution} and~\ref{fig:sine:snr_time_resolution} for reference) to synchronise an array to enable radio interferometry.
\\

Note that at higher frequencies the phase differences are phase-wrapped due to contention of the used period and the cable time delay.
Because it is symmetric for both setups, this should not affect the measurement of the signal chain time delay at the considered frequencies.
Nevertheless, the result at these frequencies must be interpreted with some caution.
\\

% Discussion
The time delays for both TD- and MD-triggered events in Figure~\ref{fig:grand:phaseshift:measurements} show a systematic behaviour of increasing total time delays for the forward setup.
However, in the backward setup, this is not as noticeable.
\\
This skewing of the channel time delays in one of the setups is also found at other frequencies (see Figures~\ref{fig:grand:phaseshift:ch1ch2} and~\ref{fig:grand:phaseshift:ch2ch4}), raising questions on the stability of the setup.
Unfortunately, it is primarily visible in the larger datasets which correspond to measurements over larger timescales.
As the number of these large datasets is limited, further investigation with the current datasets is prohibited.
\\
The skewing might also be an artefact of the short waveforms ($N\sim500\;\mathrm{samples}$) the data acquisition system was able to retrieve at the time of measurement.
Since the data acquisition system is now able to retrieve the maximum size waveforms, this systematic behaviour can be investigated in a further experiment.
\\

\begin{figure}% <<<<
	\centering
	\begin{subfigure}{0.48\textwidth}
		\includegraphics[width=\textwidth]{grand/split-cable/sine-sweep/ch1ch2fig2-combi-time-delays.pdf}
		\caption{
			Channels 1,2
		}
		\label{fig:channel-delays:1,2}
	\end{subfigure}
	\hfill
	\begin{subfigure}{0.48\textwidth}
		\includegraphics[width=\textwidth]{grand/split-cable/sine-sweep/ch2ch4fig2-combi-time-delays.pdf}
		\caption{
			Channels 2,4
		}
		\label{fig:channel-delays:2,4}
	\end{subfigure}
	\caption{
		Total (\textit{upper}) and signal chain (\textit{lower}) time delays between \subref{fig:channel-delays:1,2} channels 1 and 2, and  \subref{fig:channel-delays:2,4} 2 and 4.
		The dark grey vertical lines in the upper panes indicate the maximum measurable time delays at each frequency.
		Due to systematic effects in the measurements and a low number of samples at certain frequencies, the signal chain time delays depicted here must be taken as indicative.
		See text for discussion.
	}
	\label{fig:channel-delays}
\end{figure}% >>>>
% >>>
\end{document}
