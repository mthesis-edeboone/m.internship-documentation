% vim: fdm=marker fmr=<<<,>>>
\documentclass[../thesis.tex]{subfiles}


%%%%%
%%%%%
%%%%%

\graphicspath{
	{.}
	{../../figures/}
	{../../../figures/}
}

\begin{document}
\chapter{Air Shower Radio Interferometry}
\label{sec:interferometry}
The radio signals emitted by an \gls{EAS} (see Chapter~\ref{sec:introduction}) can be recorded by radio antennas.
For suitable frequencies, an array of radio antennas can be used as an interferometer.
Therefore, air showers can be analysed using radio interferometry.
Note that since the radio waves are mainly caused by processes involving electrons, any derived properties are tied to the electromagnetic component of the air shower.
\\

In Reference~\cite{Schoorlemmer:2020low}, a technique was developed to obtain properties of an air shower using radio interferometry.%
\footnote{
	Available as a python package at \url{https://gitlab.com/harmscho/asira}.
}
It exploits the coherent emissions in the air shower by mapping the power.
Such a power mapping (of a simulated air shower) is shown in Figure~\ref{fig:radio_air_shower}.
It reveals the air shower in one vertical and three horizontal slices.
Analysing the power mapping, we can then infer properties of the air shower such as the shower axis and $\Xmax$.
\\

The accuracy of the technique is primarily dependent on the timing accuracy of the detectors.
In Figure~\ref{fig:xmax_synchronise}, the estimated atmospheric depth resolution as a function of detector synchronisation is shown as simulated for different inclinations of the air shower.
For detector synchronisations under $2\ns$, the atmospheric depth resolution is competitive with techniques from fluorescence detectors ($\sigma(\Xmax) ~ 25\,\mathrm{g/cm^2}$ at \gls{Auger} \cite{Deligny:2023yms}).
With a difference in $\langle \Xmax \rangle$ of $\sim 100\,\mathrm{g/cm^2}$ between iron and proton initiated air showers, this depth of shower maximum resolution allows to study the mass composition of cosmic rays.
However, for worse synchronisations, the $\Xmax$ resolution for interferometry degrades linearly.
\\

An advantage of radio antennas with respect to fluorescence detectors is the increased duty-cycle.
Fluorescence detectors require clear, moonless nights, resulting in a duty-cycle of about $10\%$ whereas radio detectors have a near permanent duty-cycle.
\\

\begin{figure}
	\centering
	\begin{minipage}[t]{0.47\textwidth}
		\centering
		\includegraphics[width=\textwidth]{2006.10348/fig01.no_title}%
		\captionof{figure}{
			From \protect \cite{Schoorlemmer:2020low}.
			Radio interferometric power analysis of a simulated air shower.
			\textit{a)} shows the normalised power of $S(\vec{x})$ mapped onto a vertical planer,
				while \textit{b)}, \textit{c)} and \textit{d)} show the horizontal slices on different heights.
			On \textit{b)}, \textit{c)} and \textit{d)}, the orange and blue dot indicate the true shower axis and the maximum power respectively.
		}
		\label{fig:radio_air_shower}
	\end{minipage}
	\hfill
	\begin{minipage}[t]{0.47\textwidth}
		\centering
		\includegraphics[width=\textwidth]{2006.10348/fig03_b}%
		\captionof{figure}{
			From \protect \cite{Schoorlemmer:2020low}.
			$\Xmax$ resolution as a function of detector-to-detector synchronisation.
			Note that this figure shows a first-order effect with values particular to the antenna density of the simulated array.
		}
		\label{fig:xmax_synchronise}
	\end{minipage}
\end{figure}

\section{Radio Interferometry}
% interference: (de)coherence
Radio interferometry exploits the coherence of wave phenomena.
\\
In a radio array, each radio antenna records its ambient electric field.
A simple interferometer can be achieved by summing the recorded waveforms $S_i$ with appropriate time delays $\Delta_i(\vec{x})$ to compute the coherency of a waveform at $\vec{x}$,
\begin{equation}\label{eq:interferometric_sum}%<<<
	\phantom{.}
	S(\vec{x}, t) = \sum_i S_i(t + \Delta_i(\vec{x}))
	.
\end{equation}%>>>

% time delays: general
The time delays $\Delta_i(\vec{x})$ are dependent on the finite speed of the radio waves.
Being an electromagnetic wave, the instantaneous velocity $v$ depends solely on the refractive~index~$n$ of the medium as $v = \frac{c}{n}$.
In general, the refractive index of air is dependent on factors such as the pressure and temperature of the air the signal is passing through, and the frequencies of the signal.
\\
The time delay due to propagation can be written as
\begin{equation}\label{eq:propagation_delay}%<<<
	\phantom{,}
	\Delta_i(\vec{x}) = \frac{ \left|{ \vec{x} - \vec{a_i} }\right| }{c} n_\mathrm{eff}
	,
\end{equation}%>>>
where $n_\mathrm{eff}$ is the effective refractive index over the trajectory of the signal.
\\
% time delays: particular per antenna
Note that unlike in astronomical interferometry, the source cannot be assumed at infinity, instead it is close-by (see Figure~\ref{fig:rit_schematic}).
Therefore the time delays for each test location $\vec{x}$ have to be computed separately.
\\

% Features in S
Features in the summed waveform $S(\vec{x})$ are enhanced according to the coherence of that feature in the recorded waveforms with respect to the time delays.
\\
Figures~\ref{fig:trace_overlap:best} and~\ref{fig:trace_overlap:bad} show examples of this effect for the same recorded waveforms.
At the true source location, the recorded waveforms align and sum coherently to result in a summed waveform with enhanced features and amplitudes.
Meanwhile, at a far away location, the waveforms sum incoherently resulting in a summed waveform with low amplitudes and without clear features.
\\
% Noise suppression
An additional effect of interferometry is the suppression of noise particular to individual antennas as this adds up incoherently.
The signal in the summed waveform grows linearly with the number of detectors, while the incoherent noise in that same waveform scales with the square root of the number of detectors.
\\

\begin{figure}% fig:trace_overlap %<<<
	\centering
	\begin{minipage}[b][9cm][t]{0.47\textwidth}
		\begin{subfigure}{\textwidth}
			\includegraphics[height=8cm, width=\textwidth]{radio_interferometry/rit_schematic_far.pdf}%
			\caption{}
			\label{fig:rit_schematic}
		\end{subfigure}
	\end{minipage}\hfill%
	\begin{minipage}[b][9cm][t]{.47\textwidth}
			\vskip 1cm
		\begin{subfigure}{\textwidth}
			\includegraphics[height=2.5cm, width=\textwidth]{radio_interferometry/trace_overlap_best.png}
			\vskip 0.3cm
			\caption{}
			\label{fig:trace_overlap:best}
		\end{subfigure}
		\vskip 0.7cm
		\begin{subfigure}{\textwidth}
			\includegraphics[height=2.5cm, width=\textwidth]{radio_interferometry/trace_overlap_bad.png}
			\vskip 0.3cm
			\caption{}
			\label{fig:trace_overlap:bad}
		\end{subfigure}
	\end{minipage}
	\caption{
		Schematic of radio interferometry \subref{fig:rit_schematic}
		and the overlap between the recorded waveforms at the source location~$S_0$~\subref{fig:trace_overlap:best} and a far away location~\subref{fig:trace_overlap:bad}.
		$\Delta_i$ corresponds to the time delay per antenna from \eqref{eq:propagation_delay}.
	}
	%\hfill
	%\begin{subfigure}[t]{0.3\textwidth}
	%	\includegraphics[width=\textwidth]{radio_interferometry/trace_overlap_medium.png}
	%	\label{fig:trace_overlap:medium}
	%\end{subfigure}
	%\hfill
	%\begin{subfigure}[t]{0.3\textwidth}
	%	\includegraphics[width=\textwidth]{radio_interferometry/trace_overlap_best.png}
	%	\label{fig:trace_overlap:best}
	%\end{subfigure}
	%\label{fig:trace_overlap}
\end{figure}% >>>


% Spatial mapping of power
In the technique from \cite{Schoorlemmer:2020low}, the summed waveform $S(\vec{x})$ is computed for multiple locations.
For each location, the power in $S(\vec{x})$ is determined to create a power distribution.
%\\
An example of this power distribution of $S(\vec{x})$ is shown in Figure~\ref{fig:radio_air_shower}.
\\
The region of high power identifies strong coherent signals related to the air shower.
By mapping this region, the shower axis and shower core can be resolved.
Later, with the shower axis identified, the power along the axis is used to compute \Xmax.
\end{document}
