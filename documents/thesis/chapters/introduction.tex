% vim: fdm=marker fmr=<<<,>>>
\documentclass[../thesis.tex]{subfiles}

\graphicspath{
	{.}
	{../../figures/}
	{../../../figures/}
}

\begin{document}
\chapter{An Introduction to Cosmic Rays and Extensive Air Showers}
\label{sec:introduction}
%\section{Cosmic Particles}%<<<<<<
%<<<
% Energy and flux
The Earth is bombarded with a variety of energetic, extra-terrestrial particles.
The energies of these particles extend over many orders of magnitude (see Figure~\ref{fig:cr_flux}).
The flux of these particles decreases exponentially with increasing energy.
For very high energies, above $10^{6}\GeV$, the flux approaches one particle per~square~meter per~year, further decreasing to a single particle per~square~kilometer per~year for Ultra High Energies (UHE) at $10^{10}\GeV$.
\\

\begin{figure}%<<< fig:cr_flux
	\centering
	\includegraphics[width=0.9\textwidth]{astroparticle/The_CR_spectrum_2023.pdf}
	\caption{
		From \protect \cite{The_CR_spectrum}.
		The diffuse cosmic ray spectrum (upper line) as measured by various experiments.
		The intensity and fluxes can generally be described by rapidly decreasing power laws.
		The grey shading indicates the order of magnitude of the particle flux, such that from the ankle onwards ($E>10^9\GeV$) the flux reaches $1$~particle per~square~kilometer per~year.
	}
	\label{fig:cr_flux}
\end{figure}%>>>

% CR: magnetic field
At these high energies, the incoming particles are primarily cosmic rays\footnote{These are therefore known as \glspl{UHECR}.}, atomic nuclei typically ranging from protons ($Z=1$) up to iron ($Z=26$).
Because these are charged, the various magnetic fields they pass through will deflect and randomise their trajectories.
Of course, this effect is dependent on the strength and size of the magnetic field and the speed of the particle.
It is therefore only at the very highest energies that the direction of an initial particle might be used to (conservatively) infer the direction of its origin.
\\

% CR: galaxy / extra-galactic
The same argument (but in reverse) can be used to explain the steeper slope from the ``knee'' ($10^{6}\GeV$) onwards in Figure~\ref{fig:cr_flux}.
The acceleration of cosmic rays equally requires strong and sizeable magnetic fields.
Size constraints on the Milky~Way lead to a maximum energy for which a cosmic ray can still be contained in our galaxy.
It is thus at these energies that we can distinguish between galactic and extra-galactic origins.
\\

% Photons and Neutrinos
Other particles at these energies include photons and neutrinos, which are not charged.
Therefore, these particle types do not suffer from magnetic deflections and have the potential to reveal their source regions.
Unfortunately, aside from both being much less frequent, photons can be absorbed and created by multiple mechanisms, while neutrinos are notoriously hard to detect due to their weak interaction.
%\Todo{
%	$\gamma + \nu$ production by CR,
%	source / targets
%}
\\

%>>>
%\subsection{Air Showers}%<<<
When a cosmic ray with an energy above $10^{3}\GeV$ comes into contact with the atmosphere, secondary particles are generated, forming an \gls{EAS}.
This air shower consists of a cascade of interactions producing more particles that subsequently undergo further interactions.
Thus, the number of particles rapidly increases further down the air shower.
This happens until the mean energy per particle is sufficiently lowered from whereon these particles are absorbed in the atmosphere.
\\

Figure~\ref{fig:airshower:depth} shows the number of particles as a function of atmospheric depth where $0\;\mathrm{g/cm^2}$ corresponds with the top of the atmosphere.
The atmospheric depth at which this number of particles reaches its maximum is called $\Xmax$.
\pagebreak

In Figure~\ref{fig:airshower:depth}, $\Xmax$ is different for the air showers generated by a photon, a proton or an iron nucleus.
Typically, heavy nuclei have their first interaction higher up in the atmosphere than protons, with photons penetrating the atmosphere even further.
Therefore, accurate measurements of $\Xmax$ allow to statistically discriminate between photons, protons and iron nuclei.
\\

\begin{figure}%<<< airshower:depth
	\centering
	\vspace*{-10mm}
	\includegraphics[width=0.5\textwidth]{airshower/shower_development_depth_iron_proton_photon.pdf}
	\caption{
		From H. Schoorlemmer.
		Shower development as a function of atmospheric depth for an energy of $10^{19}\eV$.
		Typically, iron- and proton-induced air showers have a difference in $\langle \Xmax \rangle$ of $100\;\mathrm{g/cm^2}$~\cite{Deligny:2023yms}.
		For air showers from photons this is even further down the atmosphere.
		They are, however, much more rare than cosmic rays.
	}
	\label{fig:airshower:depth}
	\vspace*{-5mm}
\end{figure}%>>>

The initial particle type also influences the particle content of an air shower.
Depending on the available interaction channels, we distinguish three components in air showers: the hadronic, electromagnetic and muonic components.
Each component shows particular development and can be related to different observables of the air shower.
\\
For example, detecting a large hadronic component means the initial particle has access to hadronic interactions (creating hadrons such as pions, kaons, etc.) which is a typical sign of a cosmic ray.
In contrast, for an initial photon, which cannot interact hadronicly, the energy will be dumped into the electromagnetic part of the air shower, mainly producing electrons, positrons and photons.
\\

Finally, any charged pions created in the air shower will decay into muons while still in the atmosphere, thus comprising the muonic component.
The lifetime, and ease of penetration of relativistic muons allow them to propagate to the Earth's surface, even if other particles have decayed or have been absorbed in the atmosphere.
These are therefore prime candidates for air shower detectors on the Earth's surface.
\\

% Radio measurements
Processes in an air showers also generate radiation that can be picked up as coherent radio signals.
%% Geo Synchro
Due to the magnetic field of the Earth, the electrons in the air shower generate radiation.
Termed geomagnetic emission in Figure~\ref{fig:airshower:polarisation}, this has a polarisation that is dependent on the magnetic field vector ($\vec{B}$) and the air shower velocity ($\vec{v}$).
\\
%% Askaryan / Charge excess
An additional mechanism emitting radiation was theorised by Askaryan\cite{Askaryan:1961pfb}.
Due to the large inertia of the positively charged ions with respect to their light, negatively charged electrons, a negative charge excess is created.
In turn, this generates radiation that is polarised radially towards the shower axis (see Figure~\ref{fig:airshower:polarisation}).
\\

%% Cherenkov ring
Due to charged particles moving relativistically through the refractive atmosphere, the produced radiation is concentrated on a cone-like structure.
On the surface, this creates a ring called the Cherenkov-ring.
On this ring, a peculiar inversion happens in the time-domain of the air shower signals.
Outside the ring, radiation from the top of the air shower arrives earlier than radiation from the end of the air shower, whereas this is reversed inside the ring.
Consequently, the radiation received at the Cherenkov-ring is maximally coherent, being concentrated in a small time-window.
It is therefore crucial for radio detection to obtain measurements in this region.
\\

\begin{figure}%<<< airshower:polarisation
	\centering
	\begin{subfigure}{0.48\textwidth}
		\includegraphics[width=\textwidth]{airshower/airshower_radio_polarisation_geomagnetic.png}%
		\caption{
			Geomagnetic emission
		}
		\label{fig:airshower:polarisation:geomagnetic}
	\end{subfigure}
	\hfill
	\begin{subfigure}{0.48\textwidth}
		\includegraphics[width=\textwidth]{airshower/airshower_radio_polarisation_askaryan.png}%
		\caption{
			Askaryan or charge-excess emission
		}
		\label{fig:airshower:polarisation:askaryan}
	\end{subfigure}
	\caption{
		From \protect \cite{Schoorlemmer:2012xpa, Huege:2017bqv}
		The Radio Emission mechanisms and the resulting polarisations of the radio signal: \subref{fig:airshower:polarisation:geomagnetic} geomagnetic and \subref{fig:airshower:polarisation:askaryan} charge-excess.
		See text for explanation.
	}
	\label{fig:airshower:polarisation}
	\vspace{-2mm}
\end{figure}%>>>>>>
%>>>>>>

%\subsection{Experiments}%<<<
As mentioned, the flux at the very highest energy is in the order of one particle per square kilometer per century (see Figure~\ref{fig:cr_flux}).
Observatories therefore have to span huge areas to gather decent statistics at these highest energies on a practical timescale.
In recent and upcoming experiments, such as the~\gls{Auger}\cite{Deligny:2023yms} and the~\gls{GRAND}\cite{GRAND:2018iaj}, the approach is typically to instrument a large area with a (sparse) grid of detectors to detect the generated air shower.
With distances up to $1.5\;\mathrm{km}$ (\gls{Auger}), the detectors therefore have to operate in a self-sufficient manner with only wireless communication channels and timing provided by \gls{GNSS}.
\\

In the last two decades, with the advent of advanced electronics, the detection using radio antennas has received significant attention.
Analysing air showers using radio interferometry requires a time synchronisation of the detectors to an accuracy in the order of $1\ns$\cite{Schoorlemmer:2020low} (see Chapter~\ref{sec:interferometry} for further details).
Unfortunately, this timing accuracy is not continuously achieved by \glspl{GNSS}, if at all.
For example, in the~\gls{AERA}, this was found to range up to multiple tens of nanoseconds over the course of a single day\cite{PierreAuger:2015aqe}.
\\

\pagebreak[1]

% Structure summary
This thesis investigates a relatively straightforward method (and its limits) to improve the timing accuracy of air shower radio detectors
by using an additional radio signal called a beacon.
It is organised as follows.
\\

First, an introduction to radio interferometry is given in Chapter~\ref{sec:interferometry}.
This will be used later on and gives an insight into the timing accuracy requirements.
\\
Chapter~\ref{sec:waveform} reviews some typical techniques to analyse waveforms and to obtain timing information from them.
\\

In Chapter~\ref{sec:disciplining}, the concept of a beacon transmitter is introduced to synchronise an array of radio antennas.
It demonstrates the achievable timing accuracy for a sine and pulse beacon using the techniques described in the preceding chapter.
\\


A degeneracy in the synchronisation is encountered when the timing accuracy of the \gls{GNSS} is in the order of the periodicity of a continuous beacon.
Chapter~\ref{sec:single_sine_sync} establishes a method using a single sine wave beacon while using the radio interferometric approach to observe an air shower and correct for this effect.
\\

Finally, Chapter~\ref{sec:gnss_accuracy} investigates some possible limitations of the current hardware of \gls{GRAND} and its ability to record and reconstruct a beacon signal.
\end{document}
