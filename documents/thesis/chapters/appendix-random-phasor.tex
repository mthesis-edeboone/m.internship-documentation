% vim: fdm=marker fmr=<<<,>>>
\documentclass[../thesis.tex]{subfiles}

\graphicspath{
	{.}
	{../../figures/}
	{../../../figures/}
}

\begin{document}
\chapter{Random Phasor Sum Distribution}
\label{sec:phasor_distributions}
%\section{Random Phasor Distribution}

This section gives a short derivation of \eqref{eq:random_phasor_sum:phase:sine} using two frequency-domain phasors.
Further reading can be found in Ref.~\cite[Chapter 2.9]{goodman1985:2.9} under ``Constant Phasor plus Random Phasor Sum''.
\\

Write the noise phasor as $\vec{m} = a \, e^{i\pTrue}$ with phase $-\pi < \pTrue \leq \pi$ and amplitude $a \geq 0$,
and the signal phasor as $\vec{s} = s \, e^{i\pTrue_s}$, but rotated such that its phase $\pTrue_s = 0$.
\\
% Noise phasor description
The noise phasor is fully described by the joint probability density function
\begin{equation}
	\label{eq:noise:pdf:joint}
	\phantom{,}
	p_{A\PTrue}(a, \pTrue; \sigma)
	=
	\frac{a}{2\pi\sigma^2} e^{-\frac{a^2}{2\sigma^2}}
	,
\end{equation}
for $-\pi < \pTrue \leq \pi$ and $a \geq 0$.
\\

Integrating \eqref{eq:noise:pdf:joint} over the amplitude $a$, it follows that the phase is uniformly distributed.
\\
Likewise, the amplitude follows a Rayleigh distribution
\begin{equation}
	\label{eq:noise:pdf:amplitude}
	%\label{eq:pdf:rayleigh}
	\phantom{,}
	p_A(a; \sigma)
	%= p^{\mathrm{RICE}}_A(a; \nu = 0, \sigma)
	= \frac{a}{\sigma^2} e^{-\frac{a^2}{2\sigma^2}}
	,
\end{equation}
for which the mean is $\bar{a} = \sigma \sqrt{\frac{\pi}{2}}$ and the standard~deviation is given by $\sigma_{a} = \sigma \sqrt{ 2 - \tfrac{\pi}{2} }$.
\\

% Random phasor sum
Adding the signal phasor, the mean in \eqref{eq:noise:pdf:joint} shifts
	from $\vec{a}^2 = a^2 {\left( \cos \pTrue + \sin \pTrue \right)}^2$
	to ${\left(\vec{a} - \vec{s}\right)}^2 = {\left( a \cos \pTrue -s \right)}^2 + {\left(\sin \pTrue \right)}^2$,
	resulting in a new joint distribution
\begin{equation}
	\label{eq:phasor_sum:pdf:joint}
	\phantom{.}
	p_{A\PTrue}(a, \pTrue; s, \sigma)
	= \frac{a}{2\pi\sigma^2}
		\exp[ -
			\frac{
				{\left( a \cos \pTrue - s \right)}^2
				+ {\left( a \sin \pTrue \right)}^2
			}{
				2 \sigma^2
			}
		]
	.
\end{equation}
\\

Integrating \eqref{eq:phasor_sum:pdf:joint} over $\pTrue$ one finds
a Rice (or Rician) distribution for the amplitude,
\begin{equation}
	\label{eq:phasor_sum:pdf:amplitude}
	%\label{eq:pdf:rice}
	\phantom{,}
	p_A(a; s, \sigma)
	= \frac{a}{\sigma^2}
		\exp[-\frac{a^2 + s^2}{2\sigma^2}]
		\;
		I_0\left( \frac{a s}{\sigma^2} \right)
	,
\end{equation}
where $I_0(z)$ is the modified Bessel function of the first kind with order zero.
\\

\begin{figure}
	\centering
	\includegraphics[width=0.5\textwidth]{beacon/phasor_sum/pdfs-amplitudes.pdf}
	\caption{
		A signal phasor's amplitude in the presence of noise will follow a Rician distribution~\eqref{eq:phasor_sum:pdf:amplitude}.
		For strong signals, this approximates a gaussian distribution, while for weak signals, this approaches a Rayleigh distribution.
	}
	\label{fig:phasor_sum:pdf:amplitude}
\end{figure}
For the Rician distribution, two extreme cases can be highlighted (as can be seen in Figure~\ref{fig:phasor_sum:pdf:amplitude}).
In the case of a weak signal ($s \ll a$), \eqref{eq:phasor_sum:pdf:amplitude} behaves as a Rayleigh distribution~\eqref{eq:noise:pdf:amplitude}.
Meanwhile, it approaches a gaussian distribution around $s$ when a strong signal ($s \gg a$) is presented.
\begin{equation}
	\label{eq:strong_phasor_sum:pdf:amplitude}
	p_A(a; \sigma) = \frac{1}{\sqrt{2\pi}} \exp[-\frac{{\left(a - s\right)}^2}{2\sigma^2}]
\end{equation}\\

Like the amplitude distribution \eqref{eq:phasor_sum:pdf:amplitude}, the marginal phase distribution of \eqref{eq:phasor_sum:pdf:joint} results in two extreme cases;
weak signals correspond to the uniform distribution for \eqref{eq:noise:pdf:joint}, while strong signals are well approximated by a gaussian distribution (see Figure~\ref{fig:random_phasor_sum:pdf:phase}).
\\

The analytic form takes the following complex expression,
\begin{equation}
	\label{eq:phase_pdf:random_phasor_sum}
	p_\PTrue(\pTrue; s, \sigma) =
		\frac{ e^{-\left(\frac{s^2}{2\sigma^2}\right)} }{ 2 \pi }
		+
		\sqrt{\frac{1}{2\pi}}
		\frac{s}{\sigma}
		e^{-\left( \frac{s^2}{2\sigma^2}\sin^2{\pTrue} \right)}
		\frac{\left(
			1 + \erf{ \frac{s \cos{\pTrue}}{\sqrt{2} \sigma }}
		\right)}{2}
		\cos{\pTrue}
\end{equation}
where
\begin{equation}
	\label{eq:erf}
	\phantom{,}
	\erf{\left(z\right)} = \frac{2}{\sqrt{\pi}} \int_0^z \dif{t} e^{-t^2}
	,
\end{equation}
is the error function.
\begin{figure}
	\centering
	\includegraphics[width=0.5\textwidth]{beacon/phasor_sum/pdfs-phases.pdf}
	\caption{
		The Random Phasor Sum phase distribution \eqref{eq:phase_pdf:random_phasor_sum}.
		For strong signals, this approximates a gaussian distribution, while for weak signals, this approaches a uniform distribution.
	}
	\label{fig:random_phasor_sum:pdf:phase}
\end{figure}
\end{document}
