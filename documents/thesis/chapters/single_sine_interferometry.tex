% vim: fdm=marker fmr=<<<,>>>
\documentclass[../thesis.tex]{subfiles}

\graphicspath{
	{.}
	{../../figures/}
	{../../../figures/}
}

\begin{document}
\chapter[Single Sine Synchronisation]{Single Sine Beacon Synchronisation and Radio Interferometry}
\label{sec:single_sine_sync}

% <<<
As shown in Chapter~\ref{sec:disciplining}, both impulsive and sine beacon signals can synchronise air shower radio detectors to enable the interferometric reconstruction of extensive air showers.
This chapter will focus on using a single sine beacon to synchronise an array due to the simple setup and analysis required for such a beacon.
Additionally, at \gls{Auger}, a public TV-transmitter is broadcasting at $67.25\MHz$.
This poses an opportunity to use a ``free'' beacon to synchronise the radio antennas of \gls{AERA} and \gls{AugerPrime}.
\\

Due to the periodicity of sine beacons, the ability to synchronise an array is limited up to the beacon period $T$.
As previously mentioned, the correct periods can be ascertained by choosing a beacon period much longer than the estimated accuracy of another timing mechanism.\footnote{For reference, \gls{GNSS} timing is expected to be below $30\ns$}
Likewise, this can be achieved using the beating of multiple frequencies such as the four frequency setup in \gls{AERA}, amounting to a total period of $>1\us$.
\\

In this chapter, a different method of resolving these period mismatches is investigated by recording an impulsive signal in combination with the sine beacon.
Figure~\ref{fig:beacon_sync:sine} shows the steps of synchronisation using this combination.
The extra signal declares a shared time $\tTrueEmit$ that is common to the stations, after which the periods can be counted.
Note that the period mismatch term $\Delta k_{ij}$ in \eqref{eq:synchro_mismatch_clocks_periodic} will be referenced throughout this Chapter as $k$ since we can take station $i$ as reference ($k_i =0$).
\\

\begin{figure}%<<<
	\centering
	\begin{subfigure}[t]{0.47\textwidth}
		\includegraphics[width=\textwidth]{beacon/beacon_sync.pdf}
		\caption{
			Phase alignment
		}
		\label{fig:beacon_sync:syntonised}
	\end{subfigure}
	\hfill
	\begin{subfigure}[t]{0.47\textwidth}
		\includegraphics[width=\textwidth]{beacon/beacon_sync_period.pdf}
		\caption{
			Period alignment
		}
		\label{fig:beacon_sync:period_alignment}
	\end{subfigure}
	\caption{
		Synchronisation scheme for two antennas using a single sine wave beacon (orange) and an impulsive signal (blue).
		Vertical dashed lines indicate periods of the sine wave.
		\subref{fig:beacon_sync:syntonised} A small time delay $t_\varphi$ is derived from the phase difference of the beacon as measured by the antennas.
		\subref{fig:beacon_sync:period_alignment} The period mismatch $k$ is determined from the overlap between the impulsive signals.
		%Expecting the impulsive signals to come from the same source, the overlap between the impulsive signals is used to determine the period mismatch $k$.
		Note that the impulsive signals do not coincide perfectly due to different propagation delays from the source to the antennas.
	}
	\label{fig:beacon_sync:sine}
%	\begin{subfigure}{\textwidth}
%		\centering
%		\includegraphics[width=\textwidth]{beacon/08_beacon_sync_timing_outline.pdf}
%		\caption{
%			Measure two waveforms at different antennas at approximately the same local time (clocks are not synchronised).
%		}
%		\label{fig:beacon_sync:timing_outline}
%	\end{subfigure}
%	\begin{subfigure}{\textwidth}
%		\centering
%		\includegraphics[width=\textwidth]{beacon/08_beacon_sync_synchronised_outline.pdf}
%		\caption{
%			The beacon signal is used to remove time differences smaller than the beacon's period.
%			The detector clocks are now an unknown amount of periods out of sync.
%		}
%		\label{fig:beacon_sync:syntonised}
%	\end{subfigure}
%	\begin{subfigure}{\textwidth}
%		\centering
%		\includegraphics[width=\textwidth]{beacon/08_beacon_sync_synchronised_period_alignment.pdf}
%		\caption{
%			Lifting period degeneracy ($k=n-m=7$ periods) using the optimal overlap between impulsive signals.
%		}
%		\label{fig:beacon_sync:period_alignment}
%	\end{subfigure}
%	\caption{
%		Synchronisation scheme for two antennas using a continuous beacon and an impulsive signal, each emitted from a separate transmitter.
%		Vertical dashed lines indicate periods of the beacon (orange),
%		solid lines indicate the time of the impulsive signal (blue).
%		\\
%		\subref{fig:beacon_sync:syntonised}: The beacon allows to resolve a small timing delay ($\Delta \tClockPhase$).
%		\\
%		\subref{fig:beacon_sync:period_alignment}: Expecting the impulsive signals to come from the same source, the overlap between the two impulsive signals is used to lift the period degeneracy ($k=n-m$).
%	}
%	\label{fig:beacon_sync:sine}
\end{figure}%>>>

% Same transmitter / Static setup
When the beacon transmitter is also used to emit the signal defining $\tTrueEmit$, the number of periods $k$ can be obtained directly from the signal.
However, if this calibration signal is sent from a different location, its time delays differ from the beacon's time delays.
\\
% Dynamic setup
For static setups, these time delays can be resolved by measuring the involved distances or by taking measurements of the time delays over time.
In dynamic setups, such as for transient signals, the time delays change per event and the distances are not known a priori.
The time delays must therefore be resolved from the information of a single event.
\\

% Beacon + Impulsive -> discrete
As shown in Chapter~\ref{sec:beacon:array}, an impulsive signal allows to reconstruct the direction of origin in a single event depending on the timing resolution of the array.
Synchronising the array with a sine beacon, any clock mismatch is discretized into a number of periods $k$.
This allows to improve the reconstruction by iterating the discrete clock mismatches during reconstruction.
\\
Of course, a limit on the number of periods is required to prevent over-optimisation.
In general, they can be constrained using estimates of the accuracy of other timing mechanisms (see below).
\\
With a restricted set of allowed period shifts, we can alternate optimising the calibration signal's origin and optimising the set of period time delays of the array.
\\
% >>>

%\section{Lifting the Period Degeneracy with an Air Shower}% <<<
% <<<<
% Airshower gives t0
In the case of radio detection of air showers, the very signal of the air shower itself can be used as the calibration signal.
This falls into the dynamic setup previously mentioned.
The best period defects must thus be recovered from a single event.
\\
When doing the interferometric analysis for a sine beacon synchronised array, waveforms can only be delayed by an integer amount of periods, thereby giving discrete solutions to maximising the interferometric signal.

\clearpage
\section{Air Shower simulation}
% simulation of proton E15 on 10x10 antenna
To test the idea of combining a single sine beacon with an air shower, we simulated a set of recordings of a single air shower that also contains a beacon signal.
\footnote{
\url{https://gitlab.science.ru.nl/mthesis-edeboone/m-thesis-introduction/-/tree/main/airshower_beacon_simulation}
or
\url{https://etdeboone.nl/masters-thesis/airshower_beacon_simulation}
}
\\
The air shower signal was simulated by \acrlong{ZHAireS}\cite{Alvarez-Muniz:2010hbb} on a grid of 10x10 antennas with a spacing of $50\,\mathrm{meters}$.
Each antenna recorded a waveform of $500$ samples with a samplerate of $1\GHz$ for each of the X,Y and Z polarisations.
The air shower itself was generated by a $10^{16}\eV$ proton coming in under an angle of $20^\circ$ from zenith.
%Figure~\ref{fig:single:proton_waveform} shows the earliest and latest waveforms recorded by the array with their true time.
\\
Figure~\ref{fig:single:proton_grid} shows the maximum electric field measured at each of the antennas.
The ring of antennas with maximum electric fields in the order of $25\uVperm$ at the center of the array is the Cherenkov-ring.
The Cherenkov-ring forms due to the forward beaming of the radio emissions of the air shower.
Outside this ring, the maximum electric field quickly falls with increasing distance to the array core.
As expected for a vertical shower, the projection of the Cherenkov-ring on the ground is roughly circular.
\\

%% add beacon
A sine beacon ($\fbeacon = 51.53\MHz$) was introduced at a distance of approximately $75\,\mathrm{km}$ northwest of the array, primarily received in the X polarisation.
The distance between the antenna and the transmitter results in a phase offset with which the beacon is received at each antenna.%
\footnote{%<<<
	The beacon's amplitude is also dependent on the distance. Although simulated, this effect has not been incorporated in the analysis as it is negligible for the considered grid and distance to the transmitter.
} %>>>
The beacon signal was recorded over a longer time ($10240\,\mathrm{samples}$), to be able to distinguish the beacon and air shower later in the analysis.
\\
The final waveform of an antenna (see Figure~\ref{fig:single:proton}) was then constructed by adding its beacon and air shower waveforms and band-passing with relevant frequencies (here $30$ and $80\MHz$ are taken by default).
Of course, a gaussian white noise component is introduced to the waveform as a simple noise model (see Figure~\ref{fig:sine:snr_time_resolution} for a treatise on the timing accuracy of a sine beacon).
\\

\begin{figure}% <<<
	\centering
	\includegraphics[width=0.5\textwidth]{ZH_simulation/array_geometry_shower_amplitude.pdf}
	\caption{
		The 10x10 antenna grid used for recording the air shower.
		Colours indicate the maximum electric field recorded at the antenna.
		The Cherenkov-ring is clearly visible as a circle of radius $100\metre$ centered at $(0,0)$.
	}
	\label{fig:single:proton_grid}
\end{figure}% >>>
\begin{figure}% <<<
	\begin{subfigure}[t]{0.49\textwidth}
		\includegraphics[width=\textwidth]{ZH_simulation/ba_measure_beacon_phase.py.A74.no_mask.zoomed.pdf}
	\end{subfigure}
	\hfill
	\begin{subfigure}[t]{0.49\textwidth}
		\includegraphics[width=\textwidth]{ZH_simulation/ba_measure_beacon_phase.py.A74.fourier.pdf}
	\end{subfigure}
	\caption{
		\textit{Left:}
		%\textit{Right:}
		%Example of the earliest and latest recorded air shower waveforms in the array as simulated by ZHAireS
		Excerpt of a fully simulated waveform ($N=10240\,\mathrm{samples}$) (blue) containing the air shower (a $10^{16}\eV$~proton), the beacon (green, $\fbeacon = 51.53\MHz$) and noise.
		The part of the waveform between the vertical dashed lines is considered air shower signal and masked before measuring the beacon parameters.
		\textit{Right:}
		Fourier spectra of the waveforms.
		The green dashed lines indicate the measured beacon parameters.
		The amplitude spectrum clearly shows a strong component at roughly $50\MHz$.
		The phase spectrum of the original waveform shows the typical behaviour for a short pulse.
	}
	\label{fig:single:proton}
\end{figure}% >>>

% randomise clocks
After the creation of the antenna waveforms, the clocks are randomised by sampling a gaussian distribution with a standard deviation of $30\ns$. % (see Figure~\ref{fig:simu:sine:periods:repair_none}).
At a beacon period of $\sim 20\ns$, this ensures that multiple antennas have clock defects of at least one beacon period.
This in turn allows for synchronisation mismatches of more than one beacon period.
Moreover, it falls in the order of magnitude of clock defects that were found in \gls{AERA}\cite{PierreAuger:2015aqe}.
\\

% separate air shower from beacon
To correctly recover the beacon from the waveform, it must be separated from the air shower.
Typically, a trigger sets the location of the air shower signal in the waveform.
In our case, the air shower signal is located at $t=500\ns$ (see Figure~\ref{fig:single:proton}).
Since the beacon can be recorded for much longer than the air shower signal, we mask a window of $500$ samples around the maximum of the trace as the air shower's signal.
% measure beacon phase, remove distance phase
The remaining waveform is fed into a \gls{DTFT} \eqref{eq:fourier:dtft} to measure the beacon's phase $\pMeas$ and amplitude.
Note that due to explicitly including a time axis in a \gls{DTFT}, a number of samples can be omitted without introducing artefacts.
\\
With the obtained beacon parameters, the air shower signal is in turn reconstructed by subtracting the beacon from the full waveform in the time domain.
\\
The small clock defect $\tClockPhase$ is then finally calculated from the beacon's phase $\pMeas$ by subtracting the phase introduced by the propagation from the beacon transmitter.
\\

% introduce air shower
From the above, we now have a set of reconstructed air shower waveforms with corresponding clock defects smaller than one beacon period $T$.
Shifting the waveforms to remove these small clocks defects, we are left with resolving the correct number of periods $k$ per waveform (see Figure~\ref{fig:grid_power:repair_phases}).
\\
% >>>>
\section{\textit{k}-finding} % <<<

% unknown origin of air shower signal
Up until now, the shower axis and thus the origin of the air shower signal have not been resolved.
This means that the unknown propagation time delays for the air shower ($\tProp$) affect the alignment of the signals in Figure~\ref{fig:beacon_sync:period_alignment} in addition to the unknown clock period defects ($k T$).
As such, both this origin and the clock defects have to be determined simultaneously.
\\
% radio interferometry
If the antennas had been fully synchronised, radio interferometry as introduced in Chapter~\ref{sec:interferometry} can be applied to find the origin of the air shower signal, thus resolving the shower axis.
Still, a (rough) first estimate of the shower axis might be made using this technique or by employing other detection techniques such as those using surface or fluorescence detectors.
% (see Figure~\ref{fig:dynamic-resolve}).
\\

On the true shower axis, the waveforms would sum most coherently when the correct $k$'s are used.
Therefore, around the estimated shower axis, we define a grid search to both optimise this sum and the location of the maximum power.
In this process each waveform of the array is allowed to shift by a restricted amount of periods with respect to a reference waveform (taken to be the waveform with the highest maximum).
\\

Note that these grids are defined here in shower plane coordinates with $\vec{v}$ the true shower axis and $\vec{B}$ the local magnetic field.
Searching a grid that is slightly misaligned with the true shower axis is expected to give comparable results.
\\

The below $k$-finding algorithm is an iterative process where the grid around the shower axis is redefined on each iteration.
Discussion is found in the next Chapter.
\begin{enumerate}[start=1, label={Step \arabic*.}, ref=\arabic*, topsep=6pt, widest={Step 1.}]
	\label{algo:kfinding}
	\item \label{algo:kfinding:grid}
		Define a grid around the estimated shower axis, zooming in on each iteration.

	\item  \label{algo:kfinding:optimisation}
		$k$-optimisation: per grid point, optimise the $k$'s to maximise the sum of the waveforms (see Figure~\ref{fig:single:k-correlation}).

	\item  \label{algo:kfinding:kfinding}
		$k$-finding: find the grid point with the maximum overall sum (see Figure~\ref{fig:findks:maxima}) and select its set of $k$'s.
	
	\item \label{algo:kfinding:break}
		Stop when the set of $k$'s is equal to the set of the previous iteration, otherwise continue.

	\item \label{algo:kfinding:powermapping}
		Finally, make a power mapping with the obtained $k$'s to re-estimate the shower axis (location with maximum power) (see Figure~\ref{fig:findks:reconstruction}), and return to Step~\ref{algo:kfinding:grid} for another iteration.
\end{enumerate}
\vspace*{2pt}

Here, Step~\ref{algo:kfinding:optimisation} has been implemented by summing each waveform to the reference waveform (see above) with different time delays $kT$ and selecting the $k$ that maximises the amplitude of a waveform combination.\footnote{%<<<
	Note that one could use a correlation method instead of a maximum to select the best time delay.
	However, for simplicity and ease of computation, this has not been implemented.
	Other improvements are discussed in the next Section.
} %>>>
As shown in Figure~\ref{fig:single:k-correlation}, the maximum possible period shift has been limited to $\pm 3\,\mathrm{periods}$.
This corresponds to the maximum expected time delay between two antennas with a clock randomisation up to $30\ns$ for the considered beacon frequency.%
\footnote{
	Figure~\ref{fig:simu:error:periods} shows this is not completely true.
	However, overall, it still applies.
}

\begin{figure}%<<<
	\centering
	\includegraphics[width=0.8\textwidth]{ZH_simulation/findks/ca_period_from_shower.py.run0.i1.kfind.pdf}
	\caption{
		Finding the maximum correlation for integer period shifts (best $k=-1$) between two waveforms recording the same (simulated) air shower.
		Randomising the antenna clocks up to $30\ns$ and $\fbeacon = 51.53\MHz$ corresponds to at most $3$ periods of time difference between two waveforms.
	}
	\label{fig:single:k-correlation}
\end{figure}%>>>


\begin{figure}%<<< findks
	\begin{subfigure}[t]{0.45\textwidth}
		\includegraphics[width=\textwidth]{ZH_simulation/findks/ca_period_from_shower.py.maxima.run0.pdf}
		\caption{
			$k$-finding: optimise the $k$'s by shifting waveforms to find the maximum amplitude obtainable at each point.
		}
		\label{fig:findks:maxima}
	\end{subfigure}
	\hfill
	\begin{subfigure}[t]{0.45\textwidth}
		\includegraphics[width=\textwidth]{ZH_simulation/findks/ca_period_from_shower.py.reconstruction.run0.power.pdf}
		\caption{
			Power measurement with the $k$'s belonging to the overall maximum of the tested amplitudes.
		}
		\label{fig:findks:reconstruction}
	\end{subfigure}
	\\
	\begin{subfigure}[t]{0.45\textwidth}
		\includegraphics[width=\textwidth]{ZH_simulation/findks/ca_period_from_shower.py.maxima.run1.pdf}
		\caption{
			$2^\mathrm{nd}$ $k$-finding iteration:
				Zoom in on the location in \subref{fig:findks:reconstruction} with the highest amplitude and repeat algorithm.
		}
		\label{fig:findks:maxima:zoomed}
	\end{subfigure}
	\hfill
	\begin{subfigure}[t]{0.45\textwidth}
		\includegraphics[width=\textwidth]{ZH_simulation/findks/ca_period_from_shower.py.reconstruction.run1.power.pdf}
		\caption{
			Power measurement of the new grid.
		}
		\label{fig:findks:reconstruction:zoomed}
	\end{subfigure}
	\\
	\begin{subfigure}[t]{0.45\textwidth}
		\includegraphics[width=\textwidth]{ZH_simulation/findks/ca_period_from_shower.py.maxima.run2.pdf}
		\caption{
			$3^\mathrm{rd}$ $k$-finding iteration:
			The same set of $k$'s has been found and we stop the algorithm.
		}
		\label{fig:findks:maxima:zoomed2}
	\end{subfigure}
	\hfill
	\begin{subfigure}[t]{0.45\textwidth}
		\includegraphics[width=\textwidth]{ZH_simulation/findks/ca_period_from_shower.py.reconstruction.run2.power.pdf}
		\caption{
			Final power measurement.
		}
		\label{fig:findks:reconstruction:zoomed2}
	\end{subfigure}
	\caption{
			Iterative $k$-finding algorithm (see page~\pageref{algo:kfinding} for explanation):
		First \subref{fig:findks:maxima}, find the set of period shifts $k$ per point on a grid that returns the highest maximum amplitude (blue cross).
		The grid starts as a $8^\circ$ wide shower plane slice at $X=400\,\mathrm{g/cm^2}$, centred at the true shower axis (red cross).
		Second \subref{fig:findks:reconstruction}, perform the interferometric reconstruction with this set of period shifts.
		Zooming on the maximum power \subref{fig:findks:maxima:zoomed},\subref{fig:findks:reconstruction:zoomed} repeat the steps until the $k$'s are equal between the zoomed grids \subref{fig:findks:maxima:zoomed2},\subref{fig:findks:reconstruction:zoomed2}.
	}
	\label{fig:findks}
\end{figure}%>>>
% >>>

\clearpage
%\phantomsection
\section{Strategy / Result} %<<<

Figure~\ref{fig:grid_power_time_fixes} shows the effect of the various synchronisation stages on both the alignment of the air shower waveforms, and the interferometric power measurement near the true shower axis.
Phase synchronising the antennas gives a small increase in observed power, while further aligning the periods after the optimisation process significantly enhances this power.
\\

The initial grid plays an important role here in finding the correct axis.
Due to selecting the highest maximum amplitude, and the process above zooming in aggressively, wrong candidate axes are selected when there is no grid-location sufficiently close to the true axis.
% premature optimisation / degeneracy
Such locations are subject to differences in propagation delays that are in the order of a few beacon periods.
The restriction of the possible delays is therefore important to limit the number of potential axis locations.
\\
% fall in local extremum, maximum
In this analysis, the initial grid is defined as a very wide $8^\circ$ around the true axis.
As the number of computations scales linearly with the number of grid points ($N = N_x N_y$), it is favourable to minimise the number of grid locations.
Unfortunately, the above process has been observed to fall into local maxima when a too coarse and wide initial grid ($N_x < 13$ at $X=400\,\mathrm{g/cm^2}$) was used while restricting the time delays to $\left| k \right| \leq 3$.
\\

% Missing power / wrong k
As visible in the right side of Figure~\ref{fig:grid_power:repair_full}, not all waveforms are in sync after the optimisation.
In this case, the period defects have been resolved incorrectly for two waveforms (see Figure~\ref{fig:simu:error:periods}) due to too stringent limits on the allowable $k$'s.
Looking at Figure~\ref{fig:grid_power:repair_phases}, this was to be predicted since there are two waveforms peaking at $k=4$ from the reference waveform's peak (dashed line).
As a result, the obtained power for the resolved clock defects is slightly less than the obtained power for the true clocks.
\\

% directional reconstruction
This does not impede resolving the shower axis.
Figure~\ref{fig:grid_power:axis} shows the power mapping at four different atmospheric depths for the resolved clock defects.
Except for the low power case at $X=800\,\mathrm{g/cm^2}$, the shower axis is found to be $<0.1^\circ$ of the true shower axis.
\\

\begin{figure}% fig:simu:error
	\centering
	%\includegraphics[width=\textwidth]{ZH_simulation/cb_report_measured_antenna_offsets.py.time-amplitudes-missing-k.residuals.pdf}
	\includegraphics[width=0.5\textwidth]{ZH_simulation/cb_report_measured_antenna_time_offsets.py.time-periods.residuals.pdf}
	\caption{
		Errors in the resolved period defects with respect to the true period defects.
	}
	\label{fig:simu:error:periods}
\end{figure}

\begin{figure}%<<< grid power time fixes
	%\vspace{-2cm}
	\vspace*{-5mm}
	\centering
	\begin{subfigure}[t]{1\textwidth}
		\includegraphics[width=0.47\textwidth]{radio_interferometry/dc_grid_power_time_fixes.py.X400.repair_none.scale4d.pdf}
		\hfill
		\includegraphics[width=0.47\textwidth]{radio_interferometry/trace_overlap/on-axis/dc_grid_power_time_fixes.py.repair_none.axis.X400.trace_overlap.zoomed.repair_none.pdf}
		\vspace*{-7mm}
		\caption{
			Randomised clocks
		}
		\label{fig:grid_power:repair_none}
	\end{subfigure}
	%\hfill
	\\
	\begin{subfigure}[t]{1\textwidth}
		\includegraphics[width=0.47\textwidth]{radio_interferometry/dc_grid_power_time_fixes.py.X400.repair_phases.scale4d.pdf}
		\hfill
		\includegraphics[width=0.47\textwidth]{radio_interferometry/trace_overlap/on-axis/dc_grid_power_time_fixes.py.repair_phases.axis.X400.trace_overlap.zoomed.repair_phases.pdf}
		\vspace*{-7mm}
		\caption{
			Phase synchronisation
		}
		\label{fig:grid_power:repair_phases}
	\end{subfigure}
	\\
	\begin{subfigure}[t]{1\textwidth}
		\includegraphics[width=0.47\textwidth]{radio_interferometry/dc_grid_power_time_fixes.py.X400.repair_full.scale4d.pdf}
		\hfill
		\includegraphics[width=0.47\textwidth]{radio_interferometry/trace_overlap/on-axis/dc_grid_power_time_fixes.py.repair_full.axis.X400.trace_overlap.zoomed.repair_full.pdf}
		\vspace*{-7mm}
		\caption{
			Resolved clocks
		}
		\label{fig:grid_power:repair_full}
	\end{subfigure}
	\\
	\begin{subfigure}[t]{1\textwidth}
		\includegraphics[width=0.47\textwidth]{radio_interferometry/dc_grid_power_time_fixes.py.X400.no_offset.scale4d.pdf}
		\hfill
		\includegraphics[width=0.47\textwidth]{radio_interferometry/trace_overlap/on-axis/dc_grid_power_time_fixes.py.no_offset.axis.X400.trace_overlap.zoomed.no_offset.pdf}
		\vspace*{-7mm}
		\caption{
			True clocks
		}
		\label{fig:grid_power:no_offset}
	\end{subfigure}
	\vspace*{-7mm}
	\caption{
		Different stages of array synchronisation (unsynchronised, beacon synchronised, $k$-resolved and true clocks)
		and
		their effect on (\textit{right}) the alignment of the waveforms at the true axis
			and (\textit{left}) the interferometric power near the simulation axis (red plus).
		The maximum power is indicated by the blue cross.
		In the right panes the vertical dashed line indicates the maximum of the reference waveform.
	}
	\label{fig:grid_power_time_fixes}
\end{figure}%>>>

\pagebreak
% Future: at multiple depths
Of course, this algorithm must be evaluated at relevant atmospheric depths where the interferometric technique can resolve the air shower.
In this case, after manual inspection, the air shower was found to have \Xmax\ at roughly $400\,\mathrm{g/cm^2}$.
The algorithm is expected to perform as long as a region of strong coherent power is resolved.
This means that with the power in both Figure~\ref{fig:grid_power:axis:X200} and Figure~\ref{fig:grid_power:axis:X600}, the clock defects and air shower should be identified to the same degree.
\\

Additionally, since the true period shifts are static per event, evaluating the $k$-finding algorithm at multiple atmospheric depths allows to compare the obtained sets thereof to further minimise any incorrectly resolved period defect.
\\

Further improvements to the algorithm are foreseen in both the definition of the initial grid (Step~\ref{algo:kfinding:grid}) and the optimisation of the $k$'s (Step~\ref{algo:kfinding:optimisation}).
For example, the $k$-optimisation step currently sums the full waveform for each $k$ to find the maximum amplitude for each sum.
Instead, the timestamp of the amplitude maxima of each waveform can be compared, directly allowing to compute $k$ from the difference.
\\

Finally, from the overlapping traces in Figure~\ref{fig:grid_power:repair_full}, it is easily recognisable that some period defects have been determined incorrectly.
Inspecting Figure~\ref{fig:grid_power:repair_phases}, this was to be expected as there are two waveforms with the peak at $\left|k\right| = 4$ from the reference waveform.
Therefore, either the $k$-optimisation should have been run with a higher limit on the allowable $k$'s, or, preferably, these waveforms must be optimised after the algorithm is finished with a higher maximum $k$.

\begin{figure}%<<< grid_power:axis:X600
	\vspace*{-5mm}
	\centering
	\begin{subfigure}[t]{1\textwidth}
		\includegraphics[width=0.47\textwidth]{radio_interferometry/dc_grid_power_time_fixes.py.X200.repair_full.scale2d.pdf}
		\hfill
		\includegraphics[width=0.47\textwidth]{radio_interferometry/dc_grid_power_time_fixes.py.X200.repair_full.scale02d.pdf}
		\vspace*{-7mm}
		\caption{$X=200\,\mathrm{g/cm^2}$}
		\label{fig:grid_power:axis:X200}
	\end{subfigure}
	\begin{subfigure}[t]{1\textwidth}
		\includegraphics[width=0.47\textwidth]{radio_interferometry/dc_grid_power_time_fixes.py.X400.repair_full.scale2d.pdf}
		\hfill
		\includegraphics[width=0.47\textwidth]{radio_interferometry/dc_grid_power_time_fixes.py.X400.repair_full.scale02d.pdf}
		\vspace*{-7mm}
		\caption{$X=400\,\mathrm{g/cm^2}$}
		\label{fig:grid_power:axis:X400}
	\end{subfigure}
	\begin{subfigure}[t]{1\textwidth}
		\includegraphics[width=0.47\textwidth]{radio_interferometry/dc_grid_power_time_fixes.py.X600.repair_full.scale2d.pdf}
		\hfill
		\includegraphics[width=0.47\textwidth]{radio_interferometry/dc_grid_power_time_fixes.py.X600.repair_full.scale02d.pdf}
		\vspace*{-7mm}
		\caption{$X=600\,\mathrm{g/cm^2}$}
		\label{fig:grid_power:axis:X600}
	\end{subfigure}
	\begin{subfigure}[t]{1\textwidth}
		\includegraphics[width=0.47\textwidth]{radio_interferometry/dc_grid_power_time_fixes.py.X800.repair_full.scale2d.pdf}
		\hfill
		\includegraphics[width=0.47\textwidth]{radio_interferometry/dc_grid_power_time_fixes.py.X800.repair_full.scale02d.pdf}
		\vspace*{-7mm}
		\caption{$X=800\,\mathrm{g/cm^2}$}
		\label{fig:grid_power:axis:X800}
	\end{subfigure}
	\vspace*{-6mm}
	\caption{
		Interferometric power for the resolved clocks (from Figure~\ref{fig:grid_power:repair_full})  at four atmospheric depths for an opening angle of $2^\circ$(\textit{left}) and $0.2^\circ$(\textit{right}).
		The simulation axis is indicated by the red plus, the maximum power is indicated by the blue cross.
		Except for \subref{fig:grid_power:axis:X800} where there is no power, the shower axis is resolved within $0.1^\circ$ of the true shower axis.
	}
	\label{fig:grid_power:axis}
\end{figure}
% >>>
\end{document}
