# Interview for grant to do PhD in Paris

length: 10minutes + 10minutes questions


```
I hear from colleagues that there is a strong pressure (3 candidates for PhDs at LPNHE, and at most one candidate will be selected, if he is very good), but the fact that you passed the first stage means that they consider your academic records acceptable.
In principle for the oral examination the research part is the most important and you may then make a (positive) difference.

Hence it is critical that this is very well prepared.
The key here is to be very pedagogical, show that you understand the physics background of the work project, and how your work (past and future) fits in this. I think it would be great if we can prepare that together with Harm+me+you to increase your chances.
My suggestion is that you prepare a few slides (less than 10 given the amount of time), send them to us and we comment them. Once we reach a good version then we do at least one (ideally 2) rehearsals.
We can surely feed you with input for the slides (more details on the best practices for these kind of short presentations or details on the proposed PhD subject) to start with.
```

## Outline
 1. Airshowers / Ultra high energy / Radio Signal

 2. Radio Interferometry principle

 3. RI requires good timing -> work in internship
	mention, but put most in [backup slides]

 4. Direct Advantages of RI
	(follow airshower through the air)

 5. RI, CRs and Neutrinos
	(better direction reconstruction is interesting for Ns not for CR)
