\documentclass[showdate=false]{beamer}


%%%%%%%%
% Goal: show enthousiasm, knowledge and drive about the field

\usepackage[british]{babel}
\usepackage{amsmath}
\usepackage{hyperref}
\usepackage[backend=bibtex,style=trad-plain]{biblatex}
\usepackage{appendixnumberbeamer}
\usepackage{graphicx}
\graphicspath{{.}{./figures/}{../../figures/}}
\usepackage{todo}

\addbibresource{../../../bibliotheca/bibliography.bib}

% Disable Captions
\setbeamertemplate{caption}{\raggedright\small\insertcaption\par}

% no to navigation, yes to frame numbering
\beamertemplatenavigationsymbolsempty
\setbeamerfont{page number in head/foot}{size=\normalsize}
\setbeamertemplate{footline}[frame number]

\hypersetup{pdfpagemode=UseNone} % don't show bookmarks on initial view


\title[Early contest STEP-UP: Investigating interferometry with GRAND and BEACON]{
	{ \large Early contest STEP-UP: }\\
	{
		Investigating interferometry with\\
		GRAND\footnote{Giant Radio Array for Neutrino Detection} and BEACON\footnote{Beam forming Elevated Array for COsmic Neutrinos}
	}
}

\date{March $13^{\text{\tiny{th}}}$, 2023}

\author{
	E.T. de Boone
	\\
	\vspace{2em}
	Advisor: Olivier Martineau, LPNHE\\
	\quad\quad\quad\quad\quad Harm Schoorlemmer, IMAPP
	}

\begin{document}
{
\setbeamertemplate{footline}{} % no page number here
\section{Talk}
\frame{	\titlepage }
}

\begin{frame}{My studies}
	Studies @Radboud University, Nijmegen
	\begin{itemize}
		\item Bachelor from 2012 to 2020 \\
			\quad {\small Minor: Astrophysics}

		\item Master from 2020 to 2023 (expected) \\
			\quad {\small Specialisation: Particle and Astrophysics}\\
			\quad {\small Minor: Computational Data Science}

		\item Master's Internship (November 2021 - May 2023) \\
			\quad {\small Supervisor: Harm Schoorlemmer, IMAPP, Radboud University}\\
			\quad {\small ``Enhancing Timing Accuracy in Air Shower Radio Detectors''}
	\end{itemize}

	\vspace*{2em}

	Interests:
	\begin{itemize}
		\item Hardware experimenting
		\item Ultra High Energy particles
		\item Radio detection
	\end{itemize}
\end{frame}

% Context
%%%%%%%%%
\begin{frame}{Ultra High Energy particles}
	\begin{figure}
		\includegraphics[width=\textwidth]{grand/astroparticletypes_grand.jpg}
%		\caption{
%			From: \cite{GRAND:2018ia}
%		}
	\end{figure}
\end{frame}

\begin{frame}{Radio signals and Airshowers}
	\begin{figure}
		\includegraphics[width=\textwidth]{grand/GRAND-detection-principle-1.png}
%		\caption{
%			From: \cite{GRAND:2018ia}
%		}
	\end{figure}
\end{frame}

\begin{frame}{Advantages of Radio Interferometry}
	\begin{columns}
		\begin{column}{0.5\textwidth}
			\begin{itemize}
				\item<1-> Shower axis reconstruction%\; Relevant for $\nu$s pointing back to sources
					\vspace*{2em}
				\item<2-> Depth of airshower\\
					$\mapsto$ composition measurement (Fe, p, $\gamma$, $\nu$)
			\end{itemize}
		\end{column}
		\begin{column}{0.5\textwidth}
			\begin{figure}
				\includegraphics<1,2>[width=\textwidth]{2006.10348/fig01.png}
				\includegraphics<3>[width=\textwidth]{2006.10348/fig03_b.png}
				%\includegraphics<2>[width=\textwidth]{1607.08781/fig02b_longitudinal_shower_profile.png}
%				\caption{
%					From: \cite{Schoorlemmer:2020low}
%				}
			\end{figure}
		\end{column}
	\end{columns}
\end{frame}


% Radio Interferometry
%%%%%%%%%%%%%%%%%%%%%%
\section{Radio Interferometry Concept}
\begin{frame}{Radio Interferometry: Concept}
	\begin{columns}
		\begin{column}{0.4\textwidth}
			\begin{figure}
		   		\includegraphics[width=\textwidth]{radio_interferometry/Schematic_RIT_extracted.png}
			\end{figure}
		\end{column}
		\begin{column}{0.6\textwidth}
			\vspace*{\fill}
			\begin{itemize}
				\item<1-> Measure signal $S_i(t)$ at antenna $\vec{a_i}$

				\item<2-> Calculate light travel time \\[5pt]
					$\Delta_i(\vec{x}) = \frac{ \left| \vec{x} - \vec{a_i} \right| }{c} n_{eff}$

				\item<2-> Sum waveforms accounting \\
					for time delay \\[5pt]
					$S(\vec{x}, t) = \sum S_i( t + \Delta_i(\vec{x}) )$
			\end{itemize}

			\vspace*{\fill}

			\begin{figure}% Spatially
				\includegraphics<1>[width=0.8\textwidth]{radio_interferometry/single_trace.png}%
				\includegraphics<2>[width=0.8\textwidth]{radio_interferometry/trace_overlap_bad.png}%
				\includegraphics<3>[width=0.8\textwidth]{radio_interferometry/trace_overlap_medium.png}%
				\includegraphics<4>[width=0.8\textwidth]{radio_interferometry/trace_overlap_best.png}%
			\end{figure}
		\end{column}
	\end{columns}
\end{frame}

% My Internship
%%%%%%%%%%%%%%%
\begin{frame}{Timing Constraint for Radio Interferometry}
	\vspace*{ -2em }
	Required time accuracy $< 1 \mathrm{ns}$ not provided by GNSS $ \gtrsim 5 \mathrm{ns}$.
	\vspace{ 2em }
	\begin{columns}
		\begin{column}{0.5\textwidth}
			\visible<2->{Additional synchronisation\\
				using physics band
				}
			\begin{itemize}
				\item<2-> Pulsed beacon
				\item<2-> Long period ($\sim 1 \mathrm{\mu s}$)% (AERA)
				\item<3-> Short period ($\lesssim 20 \mathrm{ns}$)
			\end{itemize}
		\end{column}
		\begin{column}{0.5\textwidth}
			\begin{figure}% Clock error fixes
				\includegraphics<1>[width=\textwidth]{radio_interferometry/trace_overlap/dc_grid_power_time_fixes.py.scale4d.best.trace_overlap.zoomed.repair_none.png}%
				\includegraphics<2>[width=\textwidth]{radio_interferometry/trace_overlap/dc_grid_power_time_fixes.py.scale4d.best.trace_overlap.zoomed.no_offset.png}%
				\includegraphics<3>[width=\textwidth]{radio_interferometry/trace_overlap/dc_grid_power_time_fixes.py.scale4d.best.trace_overlap.zoomed.repair_phases.png}%
				\includegraphics<4>[width=\textwidth]{radio_interferometry/trace_overlap/dc_grid_power_time_fixes.py.scale4d.best.trace_overlap.zoomed.repair_all.png}%
			\end{figure}
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}{My Internship: Enhancing Timing Accuracy in Air Shower Radio Detectors}
	\vspace{1em}
	In-band mechanisms affect physics data \\
	How often should we `resynchronise'? \\

	\begin{itemize}
		\item GNSS clock stability
		\item dead-time
		\item disruptiveness
	\end{itemize}
	\vspace{1em}

	\vfill
	\begin{columns}
		\begin{column}{0.6\textwidth}
			\includegraphics<1>[width=\textwidth]{grand/split-cable/split-cable-delays-ch1ch4.pdf}
			\includegraphics<2>[width=\textwidth]{grand/split-cable/split-cable-delay-ch1ch2-50mhz-200mVpp.pdf}
		\end{column}
		\begin{column}{0.4\textwidth}
			\includegraphics[width=\textwidth]{beacon/time_res_vs_snr.pdf}
		\end{column}
	\end{columns}
\end{frame}

% Towards GRAND
%%%%%%%%%%%%%%%%%%%%

\begin{frame}{GRAND and Interferometry}
	\begin{columns}
		\begin{column}{0.6\textwidth}
			GRAND in heavy development\\
			relying on radio measurements

			\vspace{2em}
			Special interest in horizontal showers\\

			\vspace{2em}

			Neutrino's point back to source\\

			\visible<2->{
				\vspace*{\fill}
				\begin{center}
				\begin{minipage}{.6\textwidth}
					\hrule
					\centering
					\vspace{ 2em }
					\textit{Thank you!}
				\end{minipage}
				\end{center}
				%\vspace{ 4em }
			}
		\end{column}
		\begin{column}{0.4\textwidth}
			\begin{figure}
				\includegraphics<1>[width=\textwidth]{2006.10348/fig03_b.png}
				\includegraphics<2>[width=\textwidth]{2006.10348/fig01_a.png}
%				\caption{
%					From: \cite{Schoorlemmer:2020low}
%				}
			\end{figure}
		\end{column}
	\end{columns}

\end{frame}


%%%%%%%%%%%%%%%
% Backup slides
%%%%%%%%%%%%%%%
\appendix
\section{Supplemental material}
\begin{frame}[c]
	\centering
	\Large {
		\textcolor{blue} {
	Supplemental material
	}
	}
\end{frame}

\begin{frame}{Airshower development}
	\begin{figure}
		\includegraphics[width=\textwidth]{1607.08781/fig02a_airshower+detectors.png}
%		\caption{
%			From \cite{Schroder:2016hrw}
%		}
	\end{figure}
\end{frame}

\subsection{Radio Emission}
\begin{frame}{Polarised Radio Emission}
	\begin{columns}
		\begin{column}{0.2\textwidth}
			\centering
			Geosynchrotron
		\end{column}
		\begin{column}{0.7\textwidth}
			\includegraphics[width=\textwidth]{airshower/airshower_radio_polarisation_geomagnetic.png}%
		\end{column}
	\end{columns}
	\vfill
	\begin{columns}
		\begin{column}{0.2\textwidth}
			\centering
			Askaryan
		\end{column}
		\begin{column}{0.7\textwidth}
			\includegraphics[width=\textwidth]{airshower/airshower_radio_polarisation_askaryan.png}%
		\end{column}
	\end{columns}
%	\vfill
%	From: \cite{Huege:2017bqv}
\end{frame}


%%%%%%%%%
\subsection{Single frequency beacon synchronisation}
\begin{frame}{Short period beacon synchronisation}
	\begin{figure}
		\includegraphics<1>[width=\textwidth]{beacon/08_beacon_sync_timing_outline.pdf}%
		\includegraphics<2>[width=\textwidth]{beacon/08_beacon_sync_synchronised_outline.pdf}%
		\includegraphics<3>[width=\textwidth]{beacon/08_beacon_sync_synchronised_period_alignment.pdf}%
	\end{figure}
\end{frame}


\begin{frame}{Time resolving short period beacon}
	\begin{figure}
		\includegraphics<1>[width=\textwidth]{radio_interferometry/dc_grid_power_time_fixes.py.X400.repair_none.scale4d.pdf}
		\includegraphics<2>[width=\textwidth]{radio_interferometry/dc_grid_power_time_fixes.py.X400.repair_phases.scale4d.pdf}
		\includegraphics<3>[width=\textwidth]{radio_interferometry/dc_grid_power_time_fixes.py.X400.repair_all.scale4d.pdf}
		\includegraphics<4>[width=\textwidth]{radio_interferometry/dc_grid_power_time_fixes.py.X400.no_offset.scale4d.pdf}
	\end{figure}
\end{frame}

%%%%%%%%%%
\subsection{GNSS clock stability}
\begin{frame}{GNSS clock stability I}
	\begin{columns}
		\begin{column}{0.4\textwidth}
			\begin{figure}
				\centering
				\includegraphics[width=0.8\textwidth]{grand/setup/antenna-to-adc.pdf}
				\caption{
					GRAND Digitizer Unit's ADC to antennae
				}
			\end{figure}
		\end{column}
		\hfill
		\begin{column}{0.5\textwidth}
			\begin{figure}
				\includegraphics[width=\textwidth]{grand/setup/channel-delay-setup.pdf}%
				\caption{
					Channel filterchain delay experiment 
				}
			\end{figure}
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}{GNSS clock stability II}
	\begin{figure}
		\centering
		\includegraphics[width=0.7\textwidth]{grand/setup/grand-gps-setup.pdf}
		\caption{
			GNSS stability experiment
		}
	\end{figure}
\end{frame}

\subsubsection{In the field}
\begin{frame}{GNSS clock stability II}
	\begin{columns}
		\begin{column}{0.5\textwidth}
			\includegraphics[width=\textwidth]{images/IMG_20220819_154801.jpg}
		\end{column}
		\begin{column}{0.5\textwidth}
			\includegraphics[width=\textwidth]{images/IMG_20220815_161244.jpg}
		\end{column}
	\end{columns}
\end{frame}

%%%%%%%%%%%%%%
% Bibliography
%%%%%%%%%%%%%%
\section*{References}
\begin{frame}{References}
	\printbibliography
\end{frame}
\end{document}

