\documentclass[showdate=false]{beamer}

\usepackage[british]{babel}
\usepackage{amsmath}
\usepackage{hyperref}
\usepackage[backend=bibtex,style=trad-plain]{biblatex}
\usepackage{graphicx}
\graphicspath{{.}{../../figures/}}
\usepackage{todo}

\addbibresource{../../../bibliotheca/bibliography.bib}

% Disable Captions
\setbeamertemplate{caption}{\raggedright\small\insertcaption\par}

% Show Section overview at beginning of section
%\AtBeginSection[]
%{
%	\begin{frame}<beamer>{Table of Contents}
%		\tableofcontents[currentsection, currentsubsection, sectionstyle=show/shaded, subsectionstyle=hide]
%	\end{frame}
%}

% no to navigation, yes to frame numbering
\beamertemplatenavigationsymbolsempty
\setbeamerfont{page number in head/foot}{size=\normalsize}
\setbeamertemplate{footline}[frame number]

\title[Beacon Timing]{Enhancing Timing Accuracy using Beacons}
\date{Oct 06, 2022}
\author{E.T. de Boone}

\begin{document}
\frame{\titlepage}

\begin{frame}{Enhancing time accuracy}
	\begin{block}{}
		Goal: $\sigma_t < 1\mathrm{ns}$
		(enabling Radio Interferometry)
	\end{block}

	\begin{block}{Strategy}
	\begin{itemize}
		\item Simulating beacons
		\item Characterising GNSS (GRAND)
	\end{itemize}
	\end{block}

	\begin{block}{Current Timing Methods}
	\begin{itemize}
		\item GNSS (online) (GPS: $\sigma_t \leq 30 \mathrm{ns}$ $@95\%$ of the time)
		\item Beacon (online/offline)
	\end{itemize}
	\end{block}
\end{frame}

% Antenna Setup
\section{Beacon}

\begin{frame}{Antenna Setup}
	\begin{block}{}
		Local time $i$ due to time delay $t_{\mathrm{d}i}$ and clock skew $\sigma_i$\\
	\end{block}
	\begin{figure}
		\includegraphics<1>[width=0.8\textwidth]{beacon/antenna_setup_two.pdf}
		\includegraphics<2>[width=0.8\textwidth]{beacon/antenna_setup_three.pdf}
		\includegraphics<3->[width=0.8\textwidth]{beacon/antenna_setup_four.pdf}
		\vspace{-2cm}
	\end{figure}
	\begin{equation*}
		\Delta t'_{12} = t'_1 - t'_2 = \Delta t_{\mathrm{d}12} + \sigma_{12}
	\end{equation*}
	\onslide<2->\begin{equation*}
		\sigma_{12} + \sigma_{23} + \sigma_{31} = 0
	\end{equation*}
\end{frame}

\begin{frame}{Beacon properties}
	%\Todo{Pulse vs Sine and why choose one over the other}
	%Pulse:
	%	online only
	%	direct measurement of \sigma_i

	%Sine:
	%	online and offline 
	%	measurement of phase
	%	removable if f appropriate

	%\begin{table}
	%	\centering
	%	\begin{tabular}{r|l|l}
	%					& Pulse									& Sine \\
	%		\hline \\ 
	%		on/offline		& online					& online + offline \\
	%		measurement	& $t'_i (= t_i + \sigma_i)$	& $\varphi'_i (= 2\pi (\frac{t'_i}{T}\mod 1))$ \\
	%		resolving & requires high sampling rate & tracelength dependent \\
	%		removable from trace & unsure & if $f$ appropriate \\
	%	\end{tabular}
	%\end{table}

	\begin{columns}[t]
	\begin{column}{.45\textwidth}
		\begin{block}{Pulse}
		\begin{itemize}
			\item online
			\item $t'_i$ {\small $(= t_i + \sigma_i)$}
			\item resolving requires high sampling rate
		\end{itemize}
		\end{block}
	\end{column}
	\hfill
	\begin{column}{.45\textwidth}
		\begin{block}{Sine}
		\begin{itemize}
			\item online + offline
			\item $\varphi'_i$ {\small $(= 2\pi (ft'_i\mod 1))$}
			\item resolving is tracelength dependent
			\item removable from physics if $f$ appropriate
		\end{itemize}
		\end{block}
	\end{column}
	\end{columns}
\end{frame}

\subsection{Pulse}
\begin{frame}{Beacon: Pulse (single baseline)}
	\begin{figure}
		\includegraphics<1>[width=\textwidth]{beacon/field/field_single_center_time.pdf}
		\includegraphics<2>[width=\textwidth]{beacon/field/field_single_left_time.pdf}
	\end{figure}
\end{frame}
\begin{frame}{Beacon: Pulse (3 baselines)}
	\begin{figure}
		\includegraphics<1>[width=\textwidth]{beacon/field/field_three_center_time.pdf}
		\includegraphics<2>[width=\textwidth]{beacon/field/field_three_left_time.pdf}
	\end{figure}
\end{frame}

\begin{frame}{Beacon: Pulse (multi baseline)}
	\begin{figure}
		\includegraphics<1>[width=\textwidth]{beacon/field/field_square_ref0_time.pdf}
		\includegraphics<2>[width=\textwidth]{beacon/field/field_square_all_time.pdf}
	\end{figure}
\end{frame}

\subsection{Sine}
\begin{frame}{Beacon: Sine (single baseline)}
	\begin{figure}
		\includegraphics<1>[width=\textwidth]{beacon/field/field_single_center_phase.pdf}
		\includegraphics<2>[width=\textwidth]{beacon/field/field_single_left_phase.pdf}
	\end{figure}
\end{frame}
\begin{frame}{Beacon: Sine (3 baseline)}
	\begin{figure}
		\includegraphics<1>[width=\textwidth]{beacon/field/field_three_center_phase.pdf}
		\includegraphics<2>[width=\textwidth]{beacon/field/field_three_left_phase.pdf}
	\end{figure}
\end{frame}

\begin{frame}{Beacon: Sine (multi baseline reference antenna)}
	\begin{figure}
		\includegraphics<1>[width=\textwidth]{beacon/field/field_square_ref0_phase.pdf}
		\includegraphics<2>[width=\textwidth]{beacon/field/field_square_ref0_phase_zoomtx.pdf}
	\end{figure}
\end{frame}


\begin{frame}{Beacon: Sine (all baselines)}
	\begin{figure}
		\includegraphics<1>[width=\textwidth]{beacon/field/field_square_all_phase.pdf}
		\includegraphics<2>[width=\textwidth]{beacon/field/field_square_all_phase_zoomtx.pdf}
	\end{figure}
\end{frame}

\subsection{Solving Sine Beacon}
\begin{frame}{Beacon: Sine: Two traces}
	\begin{equation*}
		t'_i = (\frac{\varphi'_i}{2\pi} + n_i)T = A_i + B_i
	\end{equation*}
	\begin{figure}
		\includegraphics[width=1\textwidth]{beacon/08_beacon_sync_timing_outline.pdf}
	\end{figure}
	\begin{align*}
		\Delta t_{ij} &= (A_j + B_j) - (A_i + B_i) + \Delta t_\varphi \\
		 &= \Delta A_{ij} + \Delta t_\varphi + k_{ij}T\\
	\end{align*}
\end{frame}

\begin{frame}{Beacon: Sine: Two traces: Discrete solutions}
	\begin{figure}
		\includegraphics<1>[width=1\textwidth]{beacon/08_beacon_sync_timing_outline.pdf}
		\includegraphics<2>[width=1\textwidth]{beacon/08_beacon_sync_synchronised_period_alignment.pdf}
	\end{figure}
		\begin{figure}
			\includegraphics[width=1\textwidth]{beacon/08_beacon_sync_coherent_sum.pdf}
		\end{figure}
\end{frame}

\begin{frame}{Work in Progress}
	\begin{block}{Repeat analysis on simulated airshower (without noise)}
		\begin{enumerate}
			\item Add beacon to each antenna
			\item Assign clock offsets
		\end{enumerate}
		then determine the relative offsets between the antennas
	\end{block}
\end{frame}
\end{document}
