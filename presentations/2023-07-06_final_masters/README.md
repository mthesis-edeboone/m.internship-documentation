# Master's Final Presentation

[source](2023-Masters.tex)

## Outline

 1. Cosmic Particles from outerspace
	1. Flux to Detectors
	2. Radio Detectors (AugerPrime RD, GRAND)
	3. Radio Interferometry

 3. GPS Timing mechanism and reference timing
	1. (White Rabbit)
	2. GRAND setup

 4. Reference Beacon
	1. Common problem
	2. Pulse and SNR
	3. (Multi)Sine and SNR

 5. Single Sine Synchronisation

 6. Conclusion
	- Single Sine works: mix of radio interferometry
	- Outlook:
		1. 67MHz Auger
		2. GRAND

