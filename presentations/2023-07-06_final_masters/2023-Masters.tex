% vim: fdm=marker fmr=<<<,>>>
%\documentclass[notes]{beamer}
\documentclass[]{beamer}


%%%%%%%%%%%%%%%
% Preamble <<<
%%%%%%%%%%%%%%%

\usepackage[british]{babel}
\usepackage{csquotes}
\usepackage{amsmath}
\usepackage{hyperref}
\usepackage[backend=bibtex,style=numeric,maxnames=1]{biblatex}
\usepackage{appendixnumberbeamer}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{xurl}
\usepackage{physics}
\usepackage{cancel}
\usepackage{multicol}
\graphicspath{{.}{./figures/}{../../figures/}}
\usepackage{todo}

\addbibresource{../../bibliotheca/bibliography.bib}

% Use arXiv identifier if available
\DeclareCiteCommand{\arxivcite}
  {\usebibmacro{prenote}}
  {\usebibmacro{citeindex}%
   [\usebibmacro{cite}]
   \newunit
   \clearfield{eprintclass}
   \usebibmacro{eprint}}
  {\multicitedelim}
  {\usebibmacro{postnote}}

\newcommand{\imagesource}[1]{~\\[0pt]\vspace*{-7pt}\hspace*{10pt}{\tiny#1}}
\newcommand{\imagecredit}[1]{\imagesource{Credit:\thinspace#1}}
%\newcommand{\imagecite}[1]{\imagesource{\arxivcite{#1}}}

% Disable Captions
\setbeamertemplate{caption}{\raggedright\small\insertcaption\par}

% no to navigation, yes to frame numbering
\beamertemplatenavigationsymbolsempty
\setbeamerfont{page number in head/foot}{size=\normalsize}
\setbeamertemplate{page number in head/foot}{\insertframenumber/\inserttotalframenumber}
%\setbeamercolor{page number in head/foot}{fg=red}
\setbeamerfont{section in head/foot}{size=\small}
\setbeamercolor{section in head/foot}{fg=gray}
\setbeamertemplate{section in head/foot}{\textit{\insertsectionhead}}
%\setbeamertemplate{footline}[frame number]
\setbeamertemplate{footline}
{%
\leavevmode%
  \hbox{%
  \begin{beamercolorbox}[wd=.7\paperwidth,ht=2.55ex,dp=1ex,leftskip=1em,rightskip=1em,sep=0pt]{title in head/foot}%
    \usebeamertemplate*{section in head/foot}%
    \hfill%
  \end{beamercolorbox}
  \begin{beamercolorbox}[wd=.1\paperwidth,ht=2.55ex,dp=1ex,sep=0pt]{my empty section}
    \hfill%
  \end{beamercolorbox}
  \begin{beamercolorbox}[wd=.2\paperwidth,ht=2.55ex,dp=1ex,leftskip=1em,rightskip=1em,sep=0pt]{page number in head/foot}%
	\hfill%
    \usebeamertemplate*{page number in head/foot}%
  \end{beamercolorbox}}%
}

%% From https://tex.stackexchange.com/a/55849
% Keys to support piece-wise uncovering of elements in TikZ pictures:
% \node[visible on=<2->](foo){Foo}
% \node[visible on=<{2,4}>](bar){Bar}   % put braces around comma expressions
%
% Internally works by setting opacity=0 when invisible, which has the
% adavantage (compared to \node<2->(foo){Foo} that the node is always there, hence
% always consumes space plus that coordinate (foo) is always available.
%
% The actual command that implements the invisibility can be overriden
% by altering the style invisible. For instance \tikzsset{invisible/.style={opacity=0.2}}
% would dim the "invisible" parts. Alternatively, the color might be set to white, if the
% output driver does not support transparencies (e.g., PS)
%
\tikzset{
  invisible/.style={opacity=0},
  visible on/.style={alt={#1{}{invisible}}},
  alt/.code args={<#1>#2#3}{%
    \alt<#1>{\pgfkeysalso{#2}}{\pgfkeysalso{#3}} % \pgfkeysalso doesn't change the path
  },
}

\hypersetup{pdfpagemode=UseNone} % don't show bookmarks on initial view

% >>> Preamble
%%%%%%%%%%%%%%%
% Meta data <<<
%%%%%%%%%%%%%%%
\def\thesistitle{Enhancing Timing Accuracy\texorpdfstring{\\[0.3cm]}{ }in Air Shower Radio Detectors}
\def\thesissubtitle{}
\def\thesisauthorfirst{E.T.}
\def\thesisauthorsecond{de Boone}
\def\thesisauthoremailraw{ericteunis@deboone.nl}
\def\thesisauthoremail{\href{mailto:\thesisauthoremailraw}{\thesisauthoremailraw}}
\def\thesissupervisorfirst{dr. Harm}
\def\thesissupervisorsecond{Schoorlemmer}
\def\thesissupervisoremailraw{}
\def\thesissupervisoremail{\href{mailto:\thesissupervisoremailraw}{\thesissupervisoremailraw}}

\title[\thesistitle]{\thesistitle}

\date{July, 2023}

\author[\thesisauthorfirst\space\thesisauthorsecond]{%
	\texorpdfstring{\thesisauthorfirst\space\thesisauthorsecond\thanks{e-mail: \thesisauthoremail}\\
	\vspace*{0.5em}
	{Supervisor: \thesissupervisorfirst\space\thesissupervisorsecond }
	}{\thesisauthorfirst\space\thesisauthorsecond<\thesisauthoremailraw>}
	}

% >>> Meta data

\newcommand{\tclock}{\ensuremath{t_\mathrm{clock}}}
\newcommand{\tClock}{\tclock}
\newcommand{\ns}{\ensuremath{\mathrm{ns}}}

\newcommand{\pTrue}{\phi}
\newcommand{\PTrue}{\Phi}
\newcommand{\pMeas}{\varphi}

\newcommand{\pTrueEmit}{\pTrue_0}
\newcommand{\pTrueArriv}{\pTrueArriv'}
\newcommand{\pMeasArriv}{\pMeas_0}
\newcommand{\pProp}{\pTrue_d}
\newcommand{\pClock}{\pTrue_c}

\begin{document}
{ % Titlepage <<<
\setbeamertemplate{background}
{%
	\parbox[c][\paperheight][c]{\paperwidth}{%
		\centering%
		\vfill%
		\includegraphics[width=\textwidth]{beacon/array_setup_gps_transmitter_cows.png}%
		\vspace*{2em}
	}%
}
\setbeamertemplate{footline}{} % no page number here
\frame{	\titlepage }
} % >>>

%%%%%%%%%%%%%%%
% Start of slides <<<
%%%%%%%%%%%%%%%
\section{Cosmic Particles Detection}% <<<<
% Sources, Types, Propagation, Observables
% Flux -> Large instrumentation area
% Detection methods of Auger
%  - FD, SD
% AERA / AugerPrime RD or GRAND
\begin{frame}{Ultra High Energy particles}
	\begin{figure}
		\centering
		\includegraphics[width=0.9\textwidth]{astroparticle/bk978-0-7503-2344-4ch1f2_hr.jpg}%
		\imagecredit{Juan Antonio Aguilar and Jamie Yang. IceCube/WIPAC}
	\end{figure}
\end{frame}

\begin{frame}{Ultra High Energy particle flux}
	\begin{columns}
		\begin{column}{0.6\textwidth}
	\begin{figure}
		\centering
		%\includegraphics[width=0.7\textwidth]{astroparticle/cr_flux_PDG_2023.pdf}%
		\includegraphics[width=\textwidth]{astroparticle/spectrum.png}%
		\imagecredit{\nocite{PDG2022}Particle Data Group}
	\end{figure}
		\end{column}
		\begin{column}{0.5\textwidth}
	Large Area Experiments:\\
	%\begin{multicols}{2}
	\begin{itemize}
		\item Pierre Auger Observatory
		\item Giant Radio Array for Neutrino Detection
	\end{itemize}
		\vfill
		\begin{figure}
		\includegraphics[width=\textwidth]{images/A-schematic-of-the-Pierre-Auger-Observatory-where-each-black-dot-is-a-water-Cherenkov.png}
		\imagecredit{\href{https://www.researchgate.net/figure/A-schematic-of-the-Pierre-Auger-Observatory-where-each-black-dot-is-a-water-Cherenkov_fig1_319524774}{Hans O. Klages}}
		\end{figure}
	%\end{multicols}
		\end{column}
	\end{columns}
\end{frame}


\begin{frame}{Air Showers}
% Observables
%	\begin{columns}
%		\begin{column}{0.45\textwidth}
%	\begin{figure}
%		\includegraphics[width=\textwidth]{airshower/shower_development_depth_iron_proton_photon_with_muons.pdf}
%		\imagecredit{H. Schoorlemmer}
%	\end{figure}
%		\end{column}
%		\hfill
%		\begin{column}{0.45\textwidth}
%		\end{column}
%	\end{columns}
	\begin{figure}
		\hspace*{-2em}
		\centering
		\includegraphics[width=1.13\textwidth]{airshower/Auger_ScreenShot_GoldenHybrid1_shower_SD_FD.png}
		\imagesource{From:~\url{https://opendata.auger.org/display.php?evid=172657447200}}
	\end{figure}
\end{frame}

\begin{frame}{Air Shower Radio Emission}
	\begin{columns}
		\begin{column}{0.45\textwidth}
	\begin{figure}
		\includegraphics[width=\textwidth]{airshower/shower_development_depth_iron_proton_photon.pdf}
		\imagecredit{H. Schoorlemmer}
	\end{figure}
		\end{column}
		\hfill
		\begin{column}{0.545\textwidth}
	\begin{figure}
		\centering
		Charge excess
		\includegraphics[width=\textwidth]{airshower/airshower_radio_polarisation_askaryan.png}\\%
		\vspace*{2em}
		Geomagnetic
		\includegraphics[width=\textwidth]{airshower/airshower_radio_polarisation_geomagnetic.png}%
		\imagesource{\arxivcite{Huege:2017bqv}}
	\end{figure}
		\end{column}
	\end{columns}
\end{frame}


% >>>>
\section{Radio Interferometry}% <<<<
\begin{frame}{Radio Interferometry: Concept}
	Interferometry: Amplitude + Timing information of the $\vec{E}$-field\\
	\vspace*{ 0.8em }
	\begin{columns}
		\begin{column}{0.4\textwidth}
			\begin{figure}
				\includegraphics<1>[width=\textwidth]{radio_interferometry/rit_schematic_base.pdf}%
				\includegraphics<2>[width=\textwidth]{radio_interferometry/rit_schematic_far.pdf}%
				\includegraphics<3>[width=\textwidth]{radio_interferometry/rit_schematic_close.pdf}%
				\includegraphics<4>[width=\textwidth]{radio_interferometry/rit_schematic_true.pdf}%
			\end{figure}
		\end{column}
		\begin{column}{0.6\textwidth}
			\vspace*{\fill}
			\begin{itemize}
				\item<1-> Measure signal $S_i(t)$ at antenna $\vec{a_i}$

				\item<2-> Calculate light travel time \\[5pt]
					\quad $\Delta_i(\vec{x}) = \frac{ \left| \vec{x} - \vec{a_i} \right| }{c} n_{eff}$

				\item<2-> Sum waveforms accounting \\
					for time delay \\[5pt]
					\quad $S(\vec{x}, t) = \sum S_i( t + \Delta_i(\vec{x}) )$
			\end{itemize}
			\vspace*{\fill}
			\begin{figure}% Spatially
				\includegraphics<1>[width=0.8\textwidth]{radio_interferometry/single_trace.png}%
				\includegraphics<2>[width=0.8\textwidth]{radio_interferometry/trace_overlap_bad.png}%
				\includegraphics<3>[width=0.8\textwidth]{radio_interferometry/trace_overlap_medium.png}%
				\includegraphics<4>[width=0.8\textwidth]{radio_interferometry/trace_overlap_best.png}%
			\end{figure}
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}{Radio Interferometry: Image}
	\begin{figure}
		\centering
		\includegraphics[width=0.7\textwidth]{2006.10348/fig01.png}%
		\imagesource{\arxivcite{Schoorlemmer:2020low}}
	\end{figure}
\end{frame}

% >>>>
\section{Timing in Air Shower Radio Detectors}% <<<<
% GNSS
% reference system: White Rabbit, AERA beacon, (ADS-B?)
% GRAND setup and measurements
\begin{frame}{Timing in Air Shower Radio Detectors}
% Geometry
	Relative timing is important for Radio Interferometry. {\small ($ 1\ns\, @ c \sim 30\mathrm{cm}$)}\\
	\vspace*{1em}
	Large inter-detector spacing ($\sim 1\mathrm{km}$)\\
	$\mapsto$ Default timing mechanism: Global Navigation Satellite Systems\\
	\vspace*{1em}
	What is the accuracy of such systems?\\
	\visible<2>{
		\quad @Auger: $\sigma_t \gtrsim 10\ns$
	}
	\vfill
	\begin{columns}
		\begin{column}{0.45\textwidth}
			\begin{figure}
				\visible<2>{
				\centering
				\includegraphics[width=\textwidth]{gnss/auger/1512.02216.figure3.gnss-time-differences.png}
				\vspace*{-1em}
				\imagesource{\arxivcite{PierreAuger:2015aqe}}
				}
			\end{figure}
		\end{column}
		\hfill
		\begin{column}{0.5\textwidth}%<<<
			\vfill
			\begin{figure}
				\begin{tikzpicture}[scale=1]
					\clip (2.5 , 0) rectangle ( 6, 2.5);
					\node[anchor=south west, inner sep=0] (image) at (0,0) {\includegraphics[width=\textwidth]{beacon/array_setup_gps_transmitter_cows.png}};
					%\draw[help lines,xstep=1,ystep=1] (0,0) grid (11,5);
				\end{tikzpicture}
				\imagecredit{H. Schoorlemmer}
			\end{figure}
		\end{column}%>>>
	\end{columns}
\end{frame}

% Geometry
% Pulse method + SNR
% Sine method + SNR
\begin{frame}[t]{Timing in Radio Detectors: Beacon Synchronisation}
% Geometry
	Relative timing is important for Radio Interferometry.\\
	\vspace*{1em}
	Default Timing mechanism: {\color<1>{red} Global Navigation Satellite Systems}\\
	\visible<1->{
	+Extra Timing mechanism: {\color<1>{blue} Beacon} (Pulse, Sine)%, {\color{green} ADS-B}
	}
	\vfill
	\begin{figure}
		\hspace*{-2em}
		\begin{tikzpicture}
			\node[anchor=south west, inner sep=0] (image) at (0,0) {\includegraphics[width=0.8\textwidth]{beacon/array_setup_gps_transmitter_cows.png}};
			\begin{scope}[x={(image.south east)}, y={(image.north west)}]
				%\draw[help lines,xstep=.1,ystep=.1] (0,0) grid (1,1);
				%\foreach \x in {0,1,...,9} { \node [anchor=north] at (\x/10,0) {0.\x}; }
				%\foreach \y in {0,1,...,9} { \node [anchor=east] at (0,\y/10) {0.\y}; }
				\node (transmitter) at (0.23, 0.32) {};
				\node (gnss) at (0.85, 0.87) {};
				%% Aeroplane
				%\node[ visible on=<{2-}>] (aeroplane) at (0.5, 0.67) {\scalebox{-1}[1]{\includegraphics[width=1.5cm]{templates/aeroplane.png}}};
				%\draw[green, ultra thick, visible on=<{2-}>] (aeroplane.center)    circle[radius=8mm];
				%% Circles
				\draw[red,   ultra thick, visible on=<{1-}>] (gnss.center)         circle[radius=8mm];
				\draw[blue,  ultra thick, visible on=<{1-}>] (transmitter.center)  circle[radius=8mm];
				%% Mask Transmitter
				\fill[white, visible on=<0>] (0,0) rectangle (0.45,1) ;
			\end{scope}
		\end{tikzpicture}
		\imagecredit{H. Schoorlemmer}
	\end{figure}
\end{frame}

\section{Beacon Synchronisation}
\begin{frame}[t]{Beacon Synchronisation: Geometry}
	Local antenna time $t'_i$ due to time~delay~$t_{\mathrm{d}i}$, clock~skew~$\sigma_i$\\
	and transmitter~time~$t_\mathrm{tx}$
	\begin{equation*}
		t'_i = t_{tx} + t_{\mathrm{d}i} + \sigma_i
	\end{equation*}
	\vfill
	\begin{figure}
		\begin{tikzpicture}
			[inner sep=2mm,
			 place/.style={circle,draw=black!50,fill=white,thick}
			]
			\clip (0 , 0) rectangle (9, 2.5);
			\node[anchor=south west, inner sep=0] (image) at (0,0) {\includegraphics[width=0.8\textwidth]{beacon/array_setup_gps_transmitter_cows.png}};
			\begin{scope}[x={(image.south east)}, y={(image.north west)}]
				%\draw[help lines,xstep=.1,ystep=.1] (0,0) grid (1,1);
				%\foreach \x in {0,1,...,9} { \node [anchor=north] at (\x/10,0) {0.\x}; }
				%\foreach \y in {0,1,...,9} { \node [anchor=east] at (0,\y/10) {0.\y}; }
				%\fill[white] (0.4,0) rectangle (0.6,0.4);
				\node (transmitter) at (0.23, 0.32) {};
				\node (ant1) at (0.51, 0.32)  [place] {1};
				%\node (ant1) at (0.72, 0.25)  [place] {1};
				\node (ant2) at (0.65, 0.50)  [place] {2};
%
				\draw (transmitter.center) to node [below] {$t_{\mathrm{d}1}$} (ant1) ;
				\draw (transmitter.center) to node [above] {$t_{\mathrm{d}2}$} (ant2) ;
			\end{scope}
		\end{tikzpicture}
		\imagecredit{H. Schoorlemmer}
	\end{figure}
	\vfill

	Measured time difference:\\
	\vspace{-0.5em}
	\begin{equation*}
		\Delta t'_{12} = t'_1 - t'_2 = \Delta t_{\mathrm{d}12} + \sigma_{12} + (t_\mathrm{tx} - t_\mathrm{tx})
	\end{equation*}
\end{frame}


\subsection{Pulse Beacon}
\begin{frame}{Pulse Beacon}
	\begin{figure}
		\includegraphics[width=\textwidth]{pulse/antenna_signals_tdt0.2_zoom.pdf}
	\end{figure}
	\vfill
\end{frame}
\begin{frame}{Pulse Beacon}
	Correlation: similarity between two signals.\\
	\begin{figure}
		\includegraphics[width=\textwidth]{pulse/correlation_tdt0.2_zoom.pdf}
	\end{figure}
\end{frame}
\begin{frame}{Pulse Beacon Timing}
	\begin{figure}
		\centering
		\includegraphics[width=0.8\textwidth]{pulse/time_res_vs_snr_multiple_dt.pdf}
	\end{figure}
\end{frame}

\subsection{Sine Beacon}
\begin{frame}{(Multi)Sine Beacon}
	Phase measurement $\varphi'_i$ using Fourier Transform, $k$~unknown:
	\begin{equation*}
		 t'_i = \left[ \frac{\varphi'_i}{2\pi} \; + \; k \right] T
	\end{equation*}
	\begin{figure}
		\includegraphics[width=.45\textwidth]{methods/fourier/waveform.pdf}
		\hfill
		\includegraphics<1>[width=.45\textwidth]{methods/fourier/noisy_spectrum.pdf}
	\end{figure}
\end{frame}
\begin{frame}{(Multi)Sine Beacon Timing}
	\vspace*{1em}
	\begin{figure}
		\centering
		\includegraphics[width=0.8\textwidth]{beacon/time_res_vs_snr_large.pdf}
	\end{figure}
	\vspace*{-1em}
	\begin{columns}
		\begin{column}[b]{0.4\textwidth}
			\centering
			\tiny
			Random~Phasor~Sum:
			\autocite{goodman1985:2.9}~
			``Statistical~Optics'',
			J.~Goodman
		\end{column}
		\begin{column}[b]{0.7\textwidth}
			\tiny\begin{equation*}
	p_\PTrue(\pTrue; s, \sigma) =
		\frac{ e^{-\left(\frac{s^2}{2\sigma^2}\right)} }{ 2 \pi }
		+
		\sqrt{\frac{1}{2\pi}}
		\frac{s}{\sigma}
		e^{-\left( \frac{s^2}{2\sigma^2}\sin^2{\pTrue} \right)}
		\frac{\left(
			1 + \erf{ \frac{s \cos{\pTrue}}{\sqrt{2} \sigma }}
		\right)}{2}
		\cos{\pTrue}
			\end{equation*}
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}{Beacon Synchronisation: Conclusion}
	\vspace*{2em}
	\begin{columns}[T]
		\begin{column}{0.49\textwidth}
			\begin{center}\bfseries Pulse \end{center}
			\vspace*{-1em}
			\begin{itemize}
				\item discrete
				\item requires template
			\end{itemize}
		\end{column}
		\hfill
		\begin{column}{0.49\textwidth}
			\begin{center}\bfseries Sine \end{center}
			\vspace*{-1em}
			\begin{itemize}
				\item continuous
				\item longer trace\\ $\mapsto$ better SNR
				\item $k$ period unknown
			\end{itemize}
		\end{column}
	\end{columns}
	\vfill
	\begin{columns}
		\begin{column}{0.49\textwidth}
			\includegraphics[width=1\textwidth]{pulse/time_res_vs_snr_multiple_dt_small.pdf}
		\end{column}
		\hfill
		\begin{column}{0.49\textwidth}
			\includegraphics[width=1\textwidth]{beacon/time_res_vs_snr_small.pdf}
		\end{column}
	\end{columns}
\end{frame}

% >>>>
\section{Single Sine Synchronisation}% <<<<
% Sine method + Radio Interferometry
\begin{frame}{Single Sine Synchronisation}
	$k$ is discrete, lift the period degeneracy using the air~shower radiosignal
	\begin{equation*}
		t'_i = (\frac{\varphi'_i}{2\pi} + n_i)T = A_i + B_i
	\end{equation*}
	\vspace*{-2em}
	\begin{figure}
		%\centering
		\hspace*{-5em}
		\includegraphics<1>[width=1.3\textwidth]{beacon/08_beacon_sync_timing_outline.pdf}%
		\includegraphics<2>[width=1.3\textwidth]{beacon/08_beacon_sync_synchronised_outline.pdf}%
		\includegraphics<3>[width=1.3\textwidth]{beacon/08_beacon_sync_synchronised_period_alignment.pdf}%
	\end{figure}
	\begin{align*}
		\Delta t'_{ij} &= (A_j + B_j) - (A_i + B_i) + \Delta t'_\varphi \\
		 &= \Delta A_{ij} + \only<1>{\Delta t'_\varphi}\only<2->{\cancel{\Delta t'_\varphi}} + k_{ij}T\\
	\end{align*}
\end{frame}

\begin{frame}{Single Sine Synchronisation Simulation}
	Air Shower simulation on a grid of 100x100 antennas.
	\\
	\begin{columns}
		\begin{column}{0.45\textwidth}
			\begin{itemize}
				\item<2-> Add beacon ($T\sim20\ns$) to antenna
				\item<2-> Randomise clocks ($\sigma=30\ns$)
				\item<3-> Measure phase with DTFT
				\item<3-> Repair clocks for small offsets
				\item<3-> Iteratively find best $k_{ij}$
			\end{itemize}
		\end{column}
		\hfill
		\begin{column}{0.5\textwidth}
			\begin{figure}
				\hspace*{-2em}
				\includegraphics<1>[width=1.2\textwidth]{ZH_simulation/array_geometry_shower_amplitude.png}
				\includegraphics<2>[width=1.2\textwidth]{ZH_simulation/ba_measure_beacon_phase.py.A74.no_mask.pdf}%
				\includegraphics<3>[width=1.2\textwidth]{ZH_simulation/ba_measure_beacon_phase.py.A74.masked.pdf}%
			\end{figure}
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}{Single Sine Synchronisation: Iterative $k_{0i}$-finding}
	\small{
	``Interferometry'' while allowing to shift by $T = 1/f_\mathrm{beacon}$
	\\[5pt]
	Iterative process optimizing signal power: \\
	\; Scan positions finding the best $\{k_{0i}\}$ set,\\
	\; then evaluate on a grid near shower axis and zoom in.
	}

	\only<1-3>{\begin{figure}
	\includegraphics<1>[width=0.8\textwidth]{ZH_simulation/findks/ca_period_from_shower.py.run0.i1.zoomed.beacon.pdf}
	\includegraphics<2>[width=0.8\textwidth]{ZH_simulation/findks/ca_period_from_shower.py.maxima.run0.pdf}
	\includegraphics<3>[width=0.8\textwidth]{ZH_simulation/findks/ca_period_from_shower.py.reconstruction.run0.power.pdf}
	\end{figure}}
	\only<4>{\begin{figure}
		\includegraphics[width=0.4\textwidth]{ZH_simulation/findks/ca_period_from_shower.py.maxima.run1.pdf}
		\hfill
		\includegraphics[width=0.4\textwidth]{ZH_simulation/findks/ca_period_from_shower.py.reconstruction.run1.power.pdf}
		\vspace{0.5cm}
		\includegraphics[width=0.4\textwidth]{ZH_simulation/findks/ca_period_from_shower.py.maxima.run2.pdf}
		\hfill
		\includegraphics[width=0.4\textwidth]{ZH_simulation/findks/ca_period_from_shower.py.reconstruction.run2.power.pdf}
	\end{figure}}
\end{frame}

\begin{frame}{Single Sine Synchronisation: Timing Reparation}
	\begin{columns}
		\begin{column}{0.45\textwidth}
			{ Phase reparation }
			\includegraphics[width=\textwidth]{radio_interferometry/trace_overlap/on-axis/dc_grid_power_time_fixes.py.repair_phases.axis.trace_overlap.repair_phases.pdf}%
			\vfill
			\includegraphics[width=\textwidth]{radio_interferometry/dc_grid_power_time_fixes.py.X400.repair_phases.scale4d.pdf}%
			\label{fig:sine:repairments}
		\end{column}
		\hfill
		\begin{column}{0.45\textwidth}
			{ Phase + Period reparation }
			\includegraphics[width=\textwidth]{radio_interferometry/trace_overlap/on-axis/dc_grid_power_time_fixes.py.repair_full.axis.trace_overlap.repair_full.pdf}%
			\vfill
			\includegraphics[width=\textwidth]{radio_interferometry/dc_grid_power_time_fixes.py.X400.repair_full.scale4d.pdf}%
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}{Single Sine Synchronisation: Comparison}
	\begin{columns}
		\begin{column}{0.45\textwidth}
			{ True clock }
			\includegraphics[width=\textwidth]{radio_interferometry/trace_overlap/on-axis/dc_grid_power_time_fixes.py.no_offset.axis.trace_overlap.no_offset.pdf}%
			\vfill
			\includegraphics[width=\textwidth]{radio_interferometry/dc_grid_power_time_fixes.py.X400.no_offset.scale4d.pdf}%
		\end{column}
		\hfill
		\begin{column}{0.45\textwidth}
			{ Phase + Period reparation }
			\includegraphics[width=\textwidth]{radio_interferometry/trace_overlap/on-axis/dc_grid_power_time_fixes.py.repair_full.axis.trace_overlap.repair_full.pdf}%
			\vfill
			\includegraphics[width=\textwidth]{radio_interferometry/dc_grid_power_time_fixes.py.X400.repair_full.scale4d.pdf}%
		\end{column}
	\end{columns}
\end{frame}
% >>>>
\section{Conclusion}% <<<<
% Single Sine + Air Shower
% Outlook: Parasitic/Active vs Pulse/Sine table
% Parasitic Single Sine: 67MHz Auger
% Implementation for GRAND?
\begin{frame}{Conclusion and Outlook}
	\begin{itemize}
		\item Cosmic Particles induce Extensive Air Showers\\[10pt]
		\item Relative Timing is crucial to Radio Interferometry\\[10pt]
		\item Pulse and Sine beacons can synchronise effectively\\[10pt]
		\item Single Sine + Air Shower works
	\end{itemize}
	\vspace*{2em}

	\visible<2>{
	Outlook:
	\begin{itemize}
		\item Parasitic setups, i.e.~the $67\mathrm{MHz}$ in Auger,\\[10pt]
		\item Self-calibration using pulsed beacon
	\end{itemize}
	}
	\vfill
\end{frame}

% >>>>
% >>> End of Slides
%%%%%%%%%%%%%%%
% Backup slides <<<
%%%%%%%%%%%%%%%
\appendix
\begin{frame}[c]
	\centering
	\Large {
		\textcolor{blue} {
	Supplemental material
	}
	}
\end{frame}

\section*{Table of Contents}
\begin{frame}{Table of Contents}
	\tableofcontents
\end{frame}

\begin{frame}{Single Sine Timing Result}
	\centering
	\includegraphics<1>[width=\textwidth]{ZH_simulation/cb_report_measured_antenna_time_offsets.py.time-amplitudes.comparison.pdf}
	\includegraphics<2>[width=\textwidth]{ZH_simulation/cb_report_measured_antenna_time_offsets.py.time-amplitudes.residuals.pdf}
\end{frame}

\section{Airshower}
\begin{frame}{Airshower development}
	\begin{figure}
		\includegraphics[width=\textwidth]{1607.08781/fig02a_airshower+detectors.png}
		\imagesource{\arxivcite{Schroder:2016hrv}}
	\end{figure}
\end{frame}

\begin{frame}{Radio footprint; GRAND}
	\begin{figure}
		\includegraphics[width=0.9\textwidth]{grand/GRAND-detection-principle-1.png}
		\imagecredit{\arxivcite{GRAND:2018iaj}}
	\end{figure}
\end{frame}


\section{Radio Interferometry}
\begin{frame}{Radio Interferometry: Xmax Resolution vs Timing Resolution}
	\begin{figure}
		\centering
		\includegraphics[width=0.7\textwidth]{2006.10348/fig03_b.png}%
		\imagecredit{\arxivcite{Schoorlemmer:2020low}}
	\end{figure}
\end{frame}

\section{Beacon contamination}
\begin{frame}{Sine: Air Shower - Beacon}
	\centering
	\includegraphics[width=\textwidth]{ZH_simulation/da_reconstruction.py.traces.A74.zoomed.peak.Ex.pdf}
\end{frame}

\section{Beacon Pulse}
\begin{frame}{Filter Response and Sampling}
	\centering
	\includegraphics[width=\textwidth]{pulse/interpolation_deltapeak+antenna.pdf}
\end{frame}
%\begin{frame}{Hilbert Timing}
%	\centering
%	\includegraphics[width=\textwidth]{pulse/hilbert_timing_zoom.pdf}
%\end{frame}

\section{Beacon without TX}
\subsection{Pulse}
\begin{frame}{Beacon: Pulse (single baseline)}
	\begin{figure}
		\includegraphics<1>[width=\textwidth]{beacon/field/field_single_center_time.pdf}
		\includegraphics<2>[width=\textwidth]{beacon/field/field_single_left_time.pdf}
	\end{figure}
\end{frame}
\begin{frame}{Beacon: Pulse (3 baselines)}
	\begin{figure}
		\includegraphics<1>[width=\textwidth]{beacon/field/field_three_center_time.pdf}
		\includegraphics<2>[width=\textwidth]{beacon/field/field_three_left_time.pdf}
	\end{figure}
\end{frame}

\begin{frame}{Beacon: Pulse (multi baseline)}
	\begin{figure}
		\includegraphics<1>[width=\textwidth]{beacon/field/field_square_ref0_time.pdf}
		\includegraphics<2>[width=\textwidth]{beacon/field/field_square_all_time.pdf}
	\end{figure}
\end{frame}

\subsection{Sine}
\begin{frame}{Beacon: Sine (single baseline)}
	\begin{figure}
		\includegraphics<1>[width=\textwidth]{beacon/field/field_single_center_phase.pdf}
		\includegraphics<2>[width=\textwidth]{beacon/field/field_single_left_phase.pdf}
	\end{figure}
\end{frame}
\begin{frame}{Beacon: Sine (3 baseline)}
	\begin{figure}
		\includegraphics<1>[width=\textwidth]{beacon/field/field_three_center_phase.pdf}
		\includegraphics<2>[width=\textwidth]{beacon/field/field_three_left_phase.pdf}
	\end{figure}
\end{frame}

\begin{frame}{Beacon: Sine (multi baseline reference antenna)}
	\begin{figure}
		\includegraphics<1>[width=\textwidth]{beacon/field/field_square_ref0_phase.pdf}
		%\includegraphics<2>[width=\textwidth]{beacon/field/field_square_ref0_phase_zoomtx.pdf}
	\end{figure}
\end{frame}


\begin{frame}{Beacon: Sine (all baselines)}
	\begin{figure}
		\includegraphics<1>[width=\textwidth]{beacon/field/field_square_all_phase.pdf}
		%\includegraphics<2>[width=\textwidth]{beacon/field/field_square_all_phase_zoomtx.pdf}
	\end{figure}
\end{frame}


\section{Fourier}
\begin{frame}{DTFT vs DFT}
	\centering
	\includegraphics[width=\textwidth]{methods/fourier/noisy_spectrum.pdf}
\end{frame}
\begin{frame}{(Discrete) Fourier and Phase}
	\begin{equation*}
		\hspace{-2em}
		u(t) = \exp(i2\pi ft + \phi_t)	\xrightarrow{\mathrm{Fourier\; Transform}} f', \phi_f
	\end{equation*}
	\includegraphics[width=\textwidth]{fourier/02-fourier_phase-f_max_showcase.pdf}
\end{frame}
\begin{frame}{Phase reconstruction?}
	\begin{figure}
		\makebox[\textwidth][c]{\includegraphics[width=1.4\textwidth]{fourier/02-fourier_phase-phi_f_vs_phi_t.pdf}}%
	\end{figure}
	\begin{block}{}
		Phase reconstruction is easy if sample rate ``correct''
	\end{block}
\end{frame}

%%%%%%%%%%%%%
\begin{frame}{Phase reconstruction?}
	\begin{block}{}
		What if sample rate ``incorrect''? \\
	\end{block}
	\begin{block}<2->{}
		$\rightarrow$ Linear interpolation ({\small $f_\mathrm{signal}$, $f_\mathrm{max}$, $f_\mathrm{submax}$, $\phi_\mathrm{max}$ and $\phi_\mathrm{submax}$})
	\end{block}
	\vspace{2em}
	\begin{figure}
		\makebox[\textwidth][c]{
			\includegraphics<1-2>[width=1.4\textwidth]{fourier/02-fourier_phase-phi_f_vs_f_max_increasing_N_samples.pdf}
			\includegraphics<3>[width=1.3\textwidth]{fourier/02-fourier_phase-phase_reconstruction-unfolded.pdf}
			\includegraphics<4>[width=1.3\textwidth]{fourier/02-fourier_phase-phase_reconstruction-unfolded-zoomed.pdf}
		}%
	\end{figure}
\end{frame}




%%%%%%%%%%
\section{GNSS clock stability}
\begin{frame}{GNSS clock stability I}
	\begin{columns}
		\begin{column}{0.4\textwidth}
			\begin{figure}
				\centering
				\includegraphics[width=0.8\textwidth]{grand/setup/antenna-to-adc.pdf}
				\caption{
					GRAND Digitizer Unit's ADC to antennae
				}
			\end{figure}
		\end{column}
		\hfill
		\begin{column}{0.5\textwidth}
			\begin{figure}
				\includegraphics[width=\textwidth]{grand/setup/channel-delay-setup.pdf}%
				\caption{
					Channel filterchain delay experiment 
				}
			\end{figure}
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}{GNSS filterchain delay experiment}
	\begin{columns}
		\begin{column}{0.5\textwidth}
			\centering
			Pulse
			\includegraphics[width=\textwidth]{grand/split-cable/split-cable-delays-ch1ch4.pdf}
		\end{column}
		\begin{column}{0.5\textwidth}
			\centering
			50MHz Sinewave delay(ch1, ch2) = $46\mathrm{ps} \pm 10$
			\includegraphics[width=\textwidth]{grand/split-cable/split-cable-delay-ch1ch2-50mhz-200mVpp.pdf}
			%\includegraphics[width=\textwidth]{fourier/04_signal_to_noise_fig04.png}
		\end{column}
	\end{columns}

\end{frame}

\begin{frame}{GNSS clock stability II}
	\begin{figure}
		\centering
		\includegraphics[width=0.7\textwidth]{grand/setup/grand-gps-setup.pdf}
		\caption{
			GNSS stability experiment
		}
	\end{figure}
\end{frame}

\subsection{In the field}
\begin{frame}{}
	\centering
	\includegraphics[width=0.5\textwidth]{images/IMG_20220712_164912_grand_DU.jpg}%
	\includegraphics[width=0.5\textwidth]{images/IMG_20220712_164904_checking_gnss.jpg}%
	\vfill
	\includegraphics[width=0.5\textwidth]{images/IMG_20220819_152900.jpg}% Outside box Inside Cabling
	\includegraphics[width=0.5\textwidth]{images/flir_20220812T114019.jpg}% Heat Inside
\end{frame}

\begin{frame}{GNSS clock stability III}
	\begin{columns}
		\begin{column}{0.5\textwidth}
			\includegraphics[width=\textwidth]{images/IMG_20220819_154801.jpg}% Closed box outside
		\end{column}
		\begin{column}{0.5\textwidth}
			\includegraphics[width=\textwidth]{images/IMG_20220815_161244.jpg}% Open box outside
		\end{column}
	\end{columns}
\end{frame}

\section{White Rabbit}%<<<
\begin{frame}{Precision Time Protocol}
	\begin{itemize}
		\item Time synchronisation over (long) distance between (multiple) nodes
	\end{itemize}
	\begin{figure}
		\includegraphics[width=0.4\textwidth]{white-rabbit/protocol/ptpMSGs-color.pdf}
		\caption{
			\cite{WRPTP}
			Precision Time Protocol messages.
		}
	\end{figure}
\end{frame}
\begin{frame}{White Rabbit}
	\begin{columns}
		\begin{column}{.5\textwidth}
			White Rabbit:
			\begin{itemize}
				\item SyncE (common oscillator)
				\item PTP (synchronisation)
			\end{itemize}

			\vspace{2em}

			Factors:
			\begin{itemize}
				\item device ($\Delta_{txm}$, $\Delta_{rxs}$, ...)
				\item link ($\delta_{ms}$, ...)
			\end{itemize}
			\begin{figure}
				\makebox[\textwidth][c]{\includegraphics[width=1.1\textwidth]{white-rabbit/protocol/delaymodel.pdf}}
				\imagecredit{\autocite{WRPTP}}
			\end{figure}
		\end{column}
		\begin{column}{.5\textwidth}
			\begin{figure}
				\makebox[\textwidth][c]{\includegraphics[width=1.1\textwidth]{white-rabbit/protocol/wrptpMSGs_1.pdf}}
				\imagecredit{\autocite{WRPTP}}
			\end{figure}
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}{White Rabbit Clock Reference}
	\begin{figure}
		\centering
		\hspace*{-5em}
		\includegraphics[width=1.35\textwidth]{clocks/wr-clocks.pdf}
	\end{figure}
\end{frame}%>>>
% >>> End of Backup Slides
%%%%%%%%%%%%%%
% Bibliography <<<
%%%%%%%%%%%%%%
\section*{References}
\begin{frame}[allowframebreaks]
	\frametitle{References}
	\printbibliography
\end{frame}
% >>> Bibliography
\end{document}

