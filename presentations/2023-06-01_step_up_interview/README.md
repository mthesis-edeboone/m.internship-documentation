# Preparation for second round Interview for grant to do PhD in Paris

[source](./2023-STEP_UP.tex)
[pdf](./2023-STEP_UP.pdf)

length: 10minutes + 10minutes questions

## Outline
 1. Airshowers / Ultra high energy / Radio Signal

 2. Radio Interferometry principle

 3. RI requires good timing -> work in internship
	mention, but put most in [backup slides]

 4. Direct Advantages of RI
	(follow airshower through the air)

 5. RI, CRs and Neutrinos
	(better direction reconstruction is interesting for Ns not for CR)
