\documentclass[showdate=false]{beamer}

\usepackage[british]{babel}
\usepackage{amsmath}
\usepackage{hyperref}
\usepackage[backend=bibtex,style=trad-plain]{biblatex}
\usepackage{graphicx}
\graphicspath{{.}{../../figures/}}
\usepackage{todo}

\addbibresource{../../../bibliotheca/bibliography.bib}

% Disable Captions
\setbeamertemplate{caption}{\raggedright\small\insertcaption\par}

% Show Section overview at beginning of section
%\AtBeginSection[]
%{
%	\begin{frame}<beamer>{Table of Contents}
%		\tableofcontents[currentsection, currentsubsection, sectionstyle=show/shaded, subsectionstyle=hide]
%	\end{frame}
%}

% no to navigation, yes to frame numbering
\beamertemplatenavigationsymbolsempty
\setbeamerfont{page number in head/foot}{size=\normalsize}
\setbeamertemplate{footline}[frame number]

\title[Beacon Timing]{Enhancing Timing Accuracy using Beacons}
\date{Dec 15, 2022}
\author{E.T. de Boone}

\begin{document}
\frame{\titlepage}

\begin{frame}{Enhancing time accuracy}
	\begin{block}{}
		Goal: $\sigma_{ij} < 1\mathrm{ns}$
		(enabling Radio Interferometry)
	\end{block}

	\begin{block}{Strategy}
	\begin{itemize}
		\item Simulating beacons (both pulse and sine)
		\item Characterising GNSS (GRAND)
	\end{itemize}
	\end{block}
\end{frame}

% Antenna Setup
\section{Beacon}

\begin{frame}{Antenna Setup}
	\begin{block}{}
		Local time $t_i$ due to time delay $t_{\mathrm{d}i}$ and clock skew $\sigma_i$\\
	\end{block}
	\begin{figure}
		\includegraphics[width=0.8\textwidth]{beacon/antenna_setup_two.pdf}
	\end{figure}
	\vskip -2em
	\begin{equation*}
		\Delta t'_{12} = t'_1 - t'_2 = \Delta t_{\mathrm{d}12} + \sigma_{12} + (t_0 - t_0)
	\end{equation*}
\end{frame}

\begin{frame}{Beacon: Sine: Two traces}
	\begin{equation*}
		t'_i = (\frac{\varphi'_i}{2\pi} + n_i)T = A_i + B_i
	\end{equation*}
	\begin{figure}
		\includegraphics[width=1\textwidth]{beacon/08_beacon_sync_timing_outline.pdf}
	\end{figure}
	\begin{align*}
		\Delta t_{ij} &= (A_j + B_j) - (A_i + B_i) + \Delta t_\varphi \\
		 &= \Delta A_{ij} + \Delta t_\varphi + k_{ij}T\\
	\end{align*}
\end{frame}

\begin{frame}{Beacon: Sine: Two traces: Discrete solutions}
	\begin{figure}
		\includegraphics<1>[width=1\textwidth]{beacon/08_beacon_sync_timing_outline.pdf}
		%\includegraphics<2>[width=1\textwidth]{beacon/08_beacon_sync_synchronised_period_alignment.pdf}
	\end{figure}
		\begin{figure}
			\includegraphics[width=1\textwidth]{beacon/08_beacon_sync_coherent_sum.pdf}
		\end{figure}
\end{frame}

\begin{frame}{Simulation}
	\begin{block}{}
		Apply same steps to an airshower simulation:
		\begin{itemize}
			\item Add (sine) beacon to each antenna
			\item Shift clocks
			\item Measure phase
			\item Repair clocks for small offset
			\item Approximate
		\end{itemize}
	\end{block}
\end{frame}
\begin{frame}{}
	\begin{figure}
		\includegraphics<1>[width=1\textwidth]{figs/orig_antenna_geometry.pdf}
	\end{figure}
\end{frame}

\begin{frame}{Simulation: Phase: Local }
	\begin{block}{}
		@Antenna $i$: measure phase $\varphi_i$ , get $\varphi(\sigma_i) = \varphi_i - \varphi(t_0) - \varphi(t_{\mathrm{d}i})$
	\end{block}
	\begin{figure}
		\includegraphics<1>[width=1\textwidth]{figs/ba_measure_beacon_phase.py.A63.pdf}
		\includegraphics<2>[width=1\textwidth]{figs/ba_measure_beacon_phase.py.A63.zoomed.pdf}
		\includegraphics<3>[width=1\textwidth]{figs/bb_measure_true_phase.py.F0.05153.pdf}
	\end{figure}
\end{frame}

\begin{frame}{Simulation: Phase: Baseline}
	\begin{block}{}
		@Baseline $i,j$: $\Delta \varphi_{ij} = \varphi(\sigma_i) - \varphi(\sigma_j)$ \\

		Minimise matrix: 
		$\left(\begin{matrix}
			\Delta_{11} & \Delta_{12} & \Delta_{13} & \\
			\Delta_{21} & \Delta_{22} & \Delta_{23} & \\
			\Delta_{31} & \Delta_{32} & \Delta_{33} & \\
		\end{matrix}\right)$
	\end{block}
	\begin{figure}
		\includegraphics<1>[width=1\textwidth]{figs/bc_baseline_phase_deltas.py.0ns.1.F0.05153.pdf}
		\includegraphics<2>[width=1\textwidth]{figs/bc_baseline_phase_deltas.py.5ns_gauss1.F0.05153.pdf}
	\end{figure}
\end{frame}

\begin{frame}{Simulation: Period $k$}
	\begin{block}{}
		Interferometry while allowing to shift by $T = 1/f_\mathrm{beacon}$
	\end{block}

	\begin{figure}
	\includegraphics<1>[width=1\textwidth]{figs/ca_period_from_shower.py.loc12.0-2894.2-7780.1.i5.run2.pdf}
	\includegraphics<2>[width=1\textwidth]{figs/ca_period_from_shower.py.loc12.0-2894.2-7780.1.i5.run2.zoomed.peak.pdf}
	\includegraphics<3>[width=1\textwidth]{figs/ca_period_from_shower.py.loc12.0-2894.2-7780.1.i5.run2.zoomed.beacon.pdf}
	\includegraphics<4>[width=1\textwidth]{figs/bc_period_from_shower.py.maxima.run0.0ns.pdf}
	\end{figure}
\end{frame}

\begin{frame}{Interferometry}
	\begin{figure}
		\includegraphics<1>[width=1\textwidth]{figs/reconstruct_5ns.pdf}
		\includegraphics<2>[width=1\textwidth]{figs/reconstruct_15ns.pdf}
	\end{figure}
\end{frame}
\end{document}
