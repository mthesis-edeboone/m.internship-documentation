# Short Presentation on current status

Mostly to show what I've been doing uptil now.
Add short description of experimental setup?


### Outline
 * Timing Mechanisms (GNSS, Beacon)

 * White Rabbit as reference timing mechanism
   * White Rabbit
   * PTP

 * Fourier and Phase information
   * precise measurement $N_{sample} = N_{required}$
   * imprecise measurement $N_{sample} != N_{required}$
