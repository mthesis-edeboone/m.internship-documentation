\documentclass[showdate=false]{beamer}

\usepackage[british]{babel}
\usepackage{amsmath}
\usepackage{hyperref}
\usepackage[backend=bibtex,style=trad-plain]{biblatex}
\usepackage{graphicx}
\graphicspath{{.}{../../figures/}}

\addbibresource{../../../bibliotheca/bibliography.bib}

% Disable Captions
\setbeamertemplate{caption}{\raggedright\small\insertcaption\par}

% Show Section overview at beginning of section
\AtBeginSection[]
{
	\begin{frame}<beamer>{Table of Contents}
		\tableofcontents[currentsection, currentsubsection, sectionstyle=show/shaded, subsectionstyle=hide]
	\end{frame}
}

% no to navigation, yes to frame numbering
\beamertemplatenavigationsymbolsempty
\setbeamerfont{page number in head/foot}{size=\normalsize}
\setbeamertemplate{footline}[frame number]

\title[Timing Accuracy]{Timing Accuracy in Air Shower Detectors}
\date{February 10, 2022}
\author{E.T. de Boone}

\begin{document}
\frame{\titlepage}


\section{Timing Mechanisms in Detectors}
\begin{frame}{Timing Mechanisms}
	\begin{block}{Why improve timing accuracy?}
		\begin{itemize}
			\item Better statistics (narrow down direction of air showers)
			\item Interferometry
		\end{itemize}
	\end{block}
	\begin{block}{Strategy}
		\begin{itemize}
			\item Simulations for synchronisation techniques
			\item Characterising current methods
		\end{itemize}
	\end{block}
\end{frame}
\begin{frame}{Characterising current methods}
	\begin{block}{Current Timing Methods}
	\begin{itemize}
		\item GNSS (online)
		\item Beacon (offline)
	\end{itemize}
	\end{block}
	\vspace{2em}
	\begin{itemize}
		\item GPS Accuracy $\leq 30 \mathrm{ns}$ for $95$\% time (often better)
		\item Total time accuracy in the order of 5 -- 10~ns
		\item More accurate reference timing needed to characterise/improve current mechanisms.
	\end{itemize}

\end{frame}

%%%%%%%%%%%%%
\subsection{Beacon}
\begin{frame}{Timing Mechanisms: Beacon}
			\begin{itemize}
				\item Beating between frequency signals indicate timing
				\item PA: located in physics band $\mapsto$ offline analysis, \\
						  corrects for GPS drift.
				\item different frequency responses for antenna models and directions
			\end{itemize}
		\begin{columns}
		\begin{column}{.5\textwidth}
			\begin{figure}
				\includegraphics[width=\textwidth]{beacon/auger/1512.02216.figure2.beacon_beat.png}
				\caption{
					\cite{PierreAuger:2015aqe}
					Four beacon frequencies beating at PA.
				}
			\end{figure}
		\end{column}
		\begin{column}{.5\textwidth}
			\begin{figure}
				\includegraphics[width=\textwidth]{beacon/auger/1512.02216.figure4.ads-b.png}
				%\caption{
				%	\cite{PierreAuger:2015aqe}
				%	ADS-B and signal intercepts.
				%}
			\end{figure}
		\end{column}
	\end{columns}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Experimental Setup: White Rabbit}
\subsection[PTP]{Precision Time Protocol}
\begin{frame}{Precision Time Protocol}
	\begin{itemize}
		\item Time synchronisation over (long) distance between (multiple) nodes
	\end{itemize}
	\begin{figure}
		\includegraphics[width=0.4\textwidth]{white-rabbit/protocol/ptpMSGs-color.pdf}
		\caption{
			\cite{WRPTP}
			Precision Time Protocol messages.
		}
	\end{figure}
\end{frame}

%%%%%%%%%%%%%
\subsection[WR]{White Rabbit}
\begin{frame}{White Rabbit}
	\begin{columns}
		\begin{column}{.5\textwidth}
			White Rabbit:
			\begin{itemize}
				\item SyncE (common oscillator)
				\item PTP (synchronisation)
			\end{itemize}

			\vspace{2em}

			Factors:
			\begin{itemize}
				\item device ($\Delta_{txm}$, $\Delta_{rxs}$, ...)
				\item link ($\delta_{ms}$, ...)
			\end{itemize}
			\begin{figure}
				\makebox[\textwidth][c]{\includegraphics[width=1.1\textwidth]{white-rabbit/protocol/delaymodel.pdf}}
				%\caption{\small From \cite{WRPTP}}.
			\end{figure}
		\end{column}
		\begin{column}{.5\textwidth}
			\begin{figure}
				\makebox[\textwidth][c]{\includegraphics[width=1.1\textwidth]{white-rabbit/protocol/wrptpMSGs_1.pdf}}
				%\caption{From \cite{WRPTP}}.
			\end{figure}
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}{White Rabbit}
	\begin{figure}
		\centering
		\includegraphics<1>[width=0.8\textwidth]{gnss/phase-delocked-gps-white-rabbit-setup-colored.pdf}
		\includegraphics<2>[width=0.8\textwidth]{gnss/phase-locked-gps-white-rabbit-setup-colored.pdf}
	\end{figure}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Fourier and Phase information}
\begin{frame}{(Discrete) Fourier and Phase}
	\begin{equation*}
		\hspace{-2em}
		u(t) = \exp(i2\pi ft + \phi_t)	\xrightarrow{\mathrm{Fourier\; Transform}} f', \phi_f
	\end{equation*}
	\begin{block}{Discrete Fourier Transform}
		\begin{equation*}
			N_\mathrm{required} := f_\mathrm{sample\_rate} / f_\mathrm{signal}
		\end{equation*}
		\begin{equation*}
			f_\mathrm{Nyquist} = \frac{1}{2} f_\mathrm{sample\_rate}
		\end{equation*}
	\end{block}
	\includegraphics[width=\textwidth]{fourier/02-fourier_phase-f_max_showcase.pdf}
\end{frame}


%%%%%%%%%%%%%
\subsection{Phase reconstruction}
\begin{frame}{Phase reconstruction?}
	\begin{block}{}
		\begin{equation*}
			u(t) = \exp(2i\pi ft + \phi_t)
		\end{equation*}
	\end{block}
	\begin{figure}
		\makebox[\textwidth][c]{\includegraphics[width=1.4\textwidth]{fourier/02-fourier_phase-phi_f_vs_phi_t.pdf}}%
	\end{figure}
	\begin{block}{}
		Phase reconstruction is easy if sample rate ``correct''
	\end{block}
\end{frame}

%%%%%%%%%%%%%
\begin{frame}{Phase reconstruction?}
	\begin{block}{}
		What if sample rate ``incorrect''? \\
	\end{block}
	\begin{block}<2->{}
		$\rightarrow$ Linear interpolation ({\small $f_\mathrm{signal}$, $f_\mathrm{max}$, $f_\mathrm{submax}$, $\phi_\mathrm{max}$ and $\phi_\mathrm{submax}$})
	\end{block}
	\vspace{2em}
	\begin{figure}
		\makebox[\textwidth][c]{
			\includegraphics<1-2>[width=1.4\textwidth]{fourier/02-fourier_phase-phi_f_vs_f_max_increasing_N_samples.pdf}
			\includegraphics<3>[width=1.3\textwidth]{fourier/02-fourier_phase-phase_reconstruction-unfolded.pdf}
			\includegraphics<4>[width=1.3\textwidth]{fourier/02-fourier_phase-phase_reconstruction-unfolded-zoomed.pdf}
		}%
	\end{figure}
\end{frame}

%%%%%%%%%%%%%
\subsection{Without interpolation?}
\begin{frame}{Without interpolation? (Coming)}
	\begin{figure}
		\makebox[\textwidth][c]{\includegraphics[width=1.3\textwidth]{fourier/02-fourier_phase-relative_amplitudes_vs_N_samples_absolute.pdf}}\\%
		\makebox[\textwidth][c]{\includegraphics[width=1.3\textwidth]{fourier/02-fourier_phase-relative_amplitudes_vs_N_samples_power.pdf}}\\%
	\end{figure}
\end{frame}

%%%%%%%%%%%%%
\section*{References}
\begin{frame}{References}
		\printbibliography
\end{frame}
\end{document}
