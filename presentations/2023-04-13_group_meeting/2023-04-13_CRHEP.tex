\documentclass[showdate=false]{beamer}

\usepackage[british]{babel}
\usepackage{amsmath}
\usepackage{hyperref}
\usepackage[backend=bibtex,style=trad-plain]{biblatex}
\usepackage{graphicx}
\graphicspath{{.}{../../figures/}}
\usepackage{todo}
\usepackage{physics}
\usepackage{cancel}

\addbibresource{../../../bibliotheca/bibliography.bib}

% Disable Captions
\setbeamertemplate{caption}{\raggedright\small\insertcaption\par}

% Show Section overview at beginning of section
%\AtBeginSection[]
%{
%	\begin{frame}<beamer>{Table of Contents}
%		\tableofcontents[currentsection, currentsubsection, sectionstyle=show/shaded, subsectionstyle=hide]
%	\end{frame}
%}

% no to navigation, yes to frame numbering
\beamertemplatenavigationsymbolsempty
\setbeamerfont{page number in head/foot}{size=\normalsize}
\setbeamertemplate{footline}[frame number]

\title[Beacon Timing]{Enhancing Timing Accuracy using Beacons}
\date{Apr 13, 2023}
\author{E.T. de Boone}




\newcommand{\pTrue}{\phi}
\newcommand{\PTrue}{\Phi}
\newcommand{\pMeas}{\varphi}

\newcommand{\pTrueEmit}{\pTrue_0}
\newcommand{\pTrueArriv}{\pTrueArriv'}
\newcommand{\pMeasArriv}{\pMeas_0}
\newcommand{\pProp}{\pTrue_d}
\newcommand{\pClock}{\pTrue_c}






\begin{document}
\frame{\titlepage}

\begin{frame}{Enhancing time accuracy}
	\begin{block}{}
		Goal: $\sigma_{ij} < 1\mathrm{ns}$
		(enabling Radio Interferometry)
	\end{block}

	\begin{block}{Strategy}
	\begin{itemize}
		\item Simulating beacons (both pulse and sine)
		\item Characterising GNSS (GRAND)
	\end{itemize}
	\end{block}
\end{frame}

% Antenna Setup
\section{Beacon}
\begin{frame}{Antenna Setup}
	\vskip -2em
	Local antenna time $t'_i$ due to time delay $t_{\mathrm{d}i}$ and clock skew $\sigma_i$
	\\
	\small\begin{equation*}
		t'_i = t_{tx} + t_{\mathrm{d}i} + \sigma_i
	\end{equation*}
	\begin{figure}
		\includegraphics[width=0.6\textwidth]{beacon/antenna_setup_two.pdf}
	\end{figure}
	\vskip -2em
	\begin{equation*}
		\Delta t'_{12} = t'_1 - t'_2 = \Delta t_{\mathrm{d}12} + \sigma_{12} + (t_{tx} - t_{tx})
	\end{equation*}
\end{frame}

\begin{frame}{Beacon: Sine: Two traces}
	Required signal: sine (beacon) + single pulse
	\begin{equation*}
		t'_i = (\frac{\varphi'_i}{2\pi} + n_i)T = A_i + B_i
	\end{equation*}
	\begin{figure}
		\includegraphics<1>[width=1\textwidth]{beacon/08_beacon_sync_timing_outline.pdf}
		\includegraphics<2>[width=1\textwidth]{beacon/08_beacon_sync_synchronised_outline.pdf}
	\end{figure}
	\begin{align*}
		\Delta t'_{ij} &= (A_j + B_j) - (A_i + B_i) + \Delta t'_\varphi \\
		 &= \Delta A_{ij} + \only<1>{\Delta t'_\varphi}\only<2->{\cancel{\Delta t'_\varphi}} + k_{ij}T\\
	\end{align*}
\end{frame}

\begin{frame}{Beacon: Sine: Two traces: Discrete solutions}
	\begin{figure}
		\includegraphics<1>[width=1\textwidth]{beacon/08_beacon_sync_synchronised_outline.pdf}
		\includegraphics<2->[width=1\textwidth]{beacon/08_beacon_sync_synchronised_period_alignment.pdf}
	\end{figure}
	\begin{figure}
		\includegraphics<-2>[width=1\textwidth]{beacon/08_beacon_sync_coherent_sum.pdf}
	\end{figure}
	\only<3>{\begin{equation*}\Delta t'_{ij} = \Delta A_{ij} + \cancel{\Delta t'_\varphi} + \cancel{k_{ij}T} \end{equation*}}
	\only<3>\vfill
\end{frame}

\section{Simulations}
\begin{frame}{Simulation: Sine}
		Apply previous steps to an airshower simulation (providing the pulse):
	\begin{block}{}
		\begin{itemize}
			\item Add (sine) beacon to each antenna
			\item Shift clocks
			\item Measure phase
			\item Repair clocks for small offset $\Delta t'_{ij}$
			\item Iteratively find best $k_{ij}$
		\end{itemize}
	\end{block}
\end{frame}

\begin{frame}{Simulation: Antenna Setup}
	\begin{columns}
		\begin{column}{0.5\textwidth}
			\begin{figure}
				\includegraphics[width=\textwidth]{ZH_simulation/tx_array_geometry.png}
			\end{figure}
		\end{column}
		\hfill
		\begin{column}{0.45\textwidth}
			\begin{figure}
				\includegraphics[width=\textwidth]{ZH_simulation/array_geometry_beacon_amplitude.png}
			\end{figure}
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}{Simulation: Measure Local Phase}
	\begin{block}{}
		@Antenna $i$: measure phase $\varphi_i$ using DTFT, get $\varphi(\sigma_i) = \varphi_i - \varphi(t_0) - \varphi(t_{\mathrm{d}i})$
	\end{block}
	\begin{figure}
		\includegraphics<1>[width=0.8\textwidth]{ZH_simulation/ba_measure_beacon_phase.py.A74.no_mask.pdf}
		\includegraphics<2>[width=0.8\textwidth]{ZH_simulation/ba_measure_beacon_phase.py.A74.masked.pdf}
	\end{figure}
\end{frame}

\begin{frame}{Simulation: Phase measurement}
	Beacon frequency: $51.53~\mathrm{MHz}$
	\begin{figure}
		\includegraphics<1>[width=0.8\textwidth]{ZH_simulation/bd_antenna_phase_deltas.py.phase.residuals.c5_b_N4096_noise1e1.pdf}
		\includegraphics<2>[width=0.45\textwidth]{ZH_simulation/bd_antenna_phase_deltas.py.phase.residuals.c5_b_N4096_noise1e1.pdf}
		\hfill
		\includegraphics<2>[width=0.45\textwidth]{ZH_simulation/bd_antenna_phase_deltas.py.phase.residuals.c5_b_N4096_noise1e3.pdf}
		\\
		\vspace{0.5cm}
		\includegraphics<2>[width=0.45\textwidth]{ZH_simulation/bd_antenna_phase_deltas.py.phase.residuals.c5_b_N4096_noise1e4.pdf}
		\hfill
		\includegraphics<2>[width=0.45\textwidth]{ZH_simulation/bd_antenna_phase_deltas.py.phase.residuals.c5_b_N4096_noise1e5.pdf}
	\end{figure}
\end{frame}

\begin{frame}{Simulation: Signal to Noise}
	\begin{figure}
		\includegraphics[width=0.8\textwidth]{beacon/time_res_vs_snr.pdf}
	\end{figure}
	\begin{columns}
		\begin{column}{0.3\textwidth}
		\end{column}
		\begin{column}{0.7\textwidth}
			\tiny\begin{equation*}
	p_\PTrue(\pTrue; s, \sigma) =
		\frac{ e^{-\left(\frac{s^2}{2\sigma^2}\right)} }{ 2 \pi }
		+
		\sqrt{\frac{1}{2\pi}}
		\frac{s}{\sigma}
		e^{-\left( \frac{s^2}{2\sigma^2}\sin^2{\pTrue} \right)}
		\frac{\left(
			1 + \erf{ \frac{s \cos{\pTrue}}{\sqrt{2} \sigma }}
		\right)}{2}
		\cos{\pTrue}
			\end{equation*}
		
		\tiny{Random Phasor Sum: ``Statistical Optics'', J. Goodman}
		\end{column}
	\end{columns}
\end{frame}


\begin{frame}{Simulation: Phase: Baseline}
	\begin{block}{Correction to previous talk: modifies global phase only}
		@Baseline $i,j$: $\Delta \varphi_{ij} = \varphi(\sigma_i) - \varphi(\sigma_j)$ \\

		Minimise matrix:
		\tiny$\left(\begin{matrix}
			\Delta_{11} & \Delta_{12} & \Delta_{13} & \\
			\Delta_{21} & \Delta_{22} & \Delta_{23} & \\
			\Delta_{31} & \Delta_{32} & \Delta_{33} & \\
		\end{matrix}\right)$
	\end{block}
	\begin{figure}
		\includegraphics<1>[width=0.8\textwidth]{ZH_simulation/bc_baseline_phase_deltas.py.residuals.c5_b_N4096_noise1e3.pdf}
	\end{figure}
\end{frame}

\begin{frame}{Simulation: Period $k_i$}
	\small{
	Interferometry while allowing to shift by $T = 1/f_\mathrm{beacon}$
	\\
	Iterative process: \\
	\; Scan positions finding the best $\{k_i\}$ set, then zoom in on strongest.
	}

	\only<1-4>{\begin{figure}
	\includegraphics<1>[width=0.8\textwidth]{ZH_simulation/findks/ca_period_from_shower.py.run0.i5.loc8.0-2795.4-7816.0.pdf}
	\includegraphics<2>[width=0.8\textwidth]{ZH_simulation/findks/ca_period_from_shower.py.run0.i99.loc8.0-2795.4-7816.0.pdf}
	\includegraphics<3>[width=0.8\textwidth]{ZH_simulation/findks/ca_period_from_shower.py.maxima.run0.pdf}
	\includegraphics<4>[width=0.8\textwidth]{ZH_simulation/findks/ca_period_from_shower.py.reconstruction.run0.power.pdf}
	\end{figure}}
	\only<5>{\begin{figure}
		\includegraphics[width=0.45\textwidth]{ZH_simulation/findks/ca_period_from_shower.py.maxima.run0.pdf}
		\hfill
		\includegraphics[width=0.45\textwidth]{ZH_simulation/findks/ca_period_from_shower.py.reconstruction.run0.power.pdf}
		\vspace{0.5cm}
		\includegraphics[width=0.45\textwidth]{ZH_simulation/findks/ca_period_from_shower.py.maxima.run1.pdf}
		\hfill
		\includegraphics[width=0.45\textwidth]{ZH_simulation/findks/ca_period_from_shower.py.reconstruction.run1.power.pdf}
	\end{figure}}
\end{frame}

\begin{frame}{Simulation: Effects of Corrections}
	Found both phase and period differences
	\visible<2->{\begin{figure}
		\includegraphics[width=0.45\textwidth]{radio_interferometry/dc_grid_power_time_fixes.py.X400.repair_none.scale4d.pdf}
		\hfill
		\includegraphics[width=0.45\textwidth]{radio_interferometry/dc_grid_power_time_fixes.py.X400.repair_phases.scale4d.pdf}
		\vspace{0.5cm}
		\includegraphics[width=0.45\textwidth]{radio_interferometry/dc_grid_power_time_fixes.py.X400.repair_all.scale4d.pdf}
		\hfill
		\includegraphics[width=0.45\textwidth]{radio_interferometry/dc_grid_power_time_fixes.py.X400.no_offset.scale4d.pdf}
	\end{figure}}
\end{frame}

\begin{frame}{Simulation Conclusions}
	\begin{columns}
		\begin{column}{0.5\textwidth}
			\begin{itemize}
				\item (Single) Sine beacon:\\
					$\sigma < 1\mathrm{ns}$ from $\mathrm{SNR} > 3$\\
					depends on beacon period.
				\vspace{1cm}
				\item Pulsed beacon:\\
						(small) ongoing work\\
						while writing thesis.
			\end{itemize}
		\end{column}
		\begin{column}{0.5\textwidth}
			\begin{figure}
				\includegraphics[width=1.1\textwidth]{beacon/time_res_vs_snr.pdf}
			\end{figure}
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}{Simulation: Effects of Corrections (fullsize)}
	\begin{figure}
		\includegraphics<+>[width=\textwidth]{radio_interferometry/dc_grid_power_time_fixes.py.X400.repair_none.scale4d.pdf}
		\includegraphics<+>[width=\textwidth]{radio_interferometry/dc_grid_power_time_fixes.py.X400.repair_phases.scale4d.pdf}
		\includegraphics<+>[width=\textwidth]{radio_interferometry/dc_grid_power_time_fixes.py.X400.repair_all.scale4d.pdf}
		\includegraphics<+>[width=\textwidth]{radio_interferometry/dc_grid_power_time_fixes.py.X400.no_offset.scale4d.pdf}
	\end{figure}
\end{frame}

\begin{frame}{Signal to Noise definition}
	\begin{figure}
		\includegraphics[width=\textwidth]{ZH_simulation/signal_to_noise_definition.pdf}
	\end{figure}
\end{frame}
\end{document}
